package tls

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"os"
)

//nolint:govet // config structure
type ClientConfig struct {
	CertFile   string       `env:"CERT_FILE" yaml:"cert_file"`
	KeyFile    string       `env:"KEY_FILE" yaml:"key_file"`
	CAFiles    []string     `env:"CA_FILES" yaml:"ca_files"`
	ServerName string       `env:"SERVER_NAME" yaml:"server_name"`
	MinVersion *VersionType `env:"MIN_VERSION" yaml:"min_version"`
}

func (c *ClientConfig) TLSConfig() (*tls.Config, error) {
	var err error

	config := &tls.Config{
		ServerName: c.ServerName,
		MinVersion: tls.VersionTLS12,
	}

	if c.CertFile != "" && c.KeyFile != "" {
		config.Certificates = make([]tls.Certificate, 1)

		config.Certificates[0], err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
		if err != nil {
			return nil, fmt.Errorf("load keypair error: %v", err)
		}
	}

	if len(c.CAFiles) > 0 {
		certPool := x509.NewCertPool()

		for i := range c.CAFiles {
			var pem []byte

			if pem, err = os.ReadFile(c.CAFiles[i]); err != nil {
				return nil, fmt.Errorf("read CA cert file error: %v", err)
			}

			certPool.AppendCertsFromPEM(pem)
		}

		config.RootCAs = certPool
	}

	if c.MinVersion != nil {
		config.MinVersion = uint16(*c.MinVersion)
	}

	return config, nil
}
