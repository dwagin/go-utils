package tls

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"os"
)

//nolint:govet // config structure
type ServerConfig struct {
	CertFile       string          `env:"CERT_FILE" yaml:"cert_file"`
	KeyFile        string          `env:"KEY_FILE" yaml:"key_file"`
	ClientAuthType *ClientAuthType `env:"CLIENT_AUTH_TYPE" yaml:"client_auth_type"`
	ClientCAs      []string        `env:"CLIENT_CA_FILES" yaml:"client_ca_files"`
	MinVersion     *VersionType    `env:"MIN_VERSION" yaml:"min_version"`
}

func (c *ServerConfig) TLSConfig() (*tls.Config, error) {
	var err error

	if c.CertFile == "" || c.KeyFile == "" {
		return nil, errors.New("'cert_file' and 'key_file' must be defined")
	}

	config := &tls.Config{
		Certificates: make([]tls.Certificate, 1),
		ClientAuth:   tls.NoClientCert,
		MinVersion:   tls.VersionTLS12,
	}

	config.Certificates[0], err = tls.LoadX509KeyPair(c.CertFile, c.KeyFile)
	if err != nil {
		return nil, fmt.Errorf("load keypair error: %v", err)
	}

	if c.ClientAuthType != nil {
		config.ClientAuth = tls.ClientAuthType(*c.ClientAuthType)
	}

	if len(c.ClientCAs) > 0 && config.ClientAuth == tls.NoClientCert {
		return nil, errors.New("'client_ca_files' have been configured without a 'client_auth_type'")
	}

	if len(c.ClientCAs) > 0 {
		certPool := x509.NewCertPool()

		for i := range c.ClientCAs {
			var (
				fileName = c.ClientCAs[i]
				pem      []byte
			)

			if pem, err = os.ReadFile(fileName); err != nil {
				return nil, fmt.Errorf("read CA cert file error: %v", err)
			}

			certPool.AppendCertsFromPEM(pem)
		}

		config.ClientCAs = certPool
	}

	if c.MinVersion != nil {
		config.MinVersion = uint16(*c.MinVersion)
	}

	return config, nil
}
