package tls_test

import (
	"crypto/tls"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

func TestClientAuthType_UnmarshalText(t *testing.T) {
	t.Parallel()

	testCases := []struct { //nolint:govet // tests
		name        string
		value       []byte
		expected    utilsTLS.ClientAuthType
		expectedErr error
	}{
		{
			name:     "RequestClientCert",
			value:    []byte("RequestClientCert"),
			expected: utilsTLS.ClientAuthType(tls.RequestClientCert),
		},
		{
			name:     "request_client_cert",
			value:    []byte("request_client_cert"),
			expected: utilsTLS.ClientAuthType(tls.RequestClientCert),
		},
		{
			name:     "RequireClientCert",
			value:    []byte("RequireClientCert"),
			expected: utilsTLS.ClientAuthType(tls.RequireAnyClientCert),
		},
		{
			name:     "require_client_cert",
			value:    []byte("require_client_cert"),
			expected: utilsTLS.ClientAuthType(tls.RequireAnyClientCert),
		},
		{
			name:     "VerifyClientCertIfGiven",
			value:    []byte("VerifyClientCertIfGiven"),
			expected: utilsTLS.ClientAuthType(tls.VerifyClientCertIfGiven),
		},
		{
			name:     "verify_client_cert_if_given",
			value:    []byte("verify_client_cert_if_given"),
			expected: utilsTLS.ClientAuthType(tls.VerifyClientCertIfGiven),
		},
		{
			name:     "RequireAndVerifyClientCert",
			value:    []byte("RequireAndVerifyClientCert"),
			expected: utilsTLS.ClientAuthType(tls.RequireAndVerifyClientCert),
		},
		{
			name:     "require_and_verify_client_cert",
			value:    []byte("require_and_verify_client_cert"),
			expected: utilsTLS.ClientAuthType(tls.RequireAndVerifyClientCert),
		},
		{
			name:        "Unknown",
			value:       []byte("Unknown"),
			expectedErr: errors.New("unknown ClientAuthType"),
		},
	}

	for i := range testCases {
		test := testCases[i]

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			require := require.New(t)

			var actual utilsTLS.ClientAuthType

			err := actual.UnmarshalText(test.value)
			if test.expectedErr == nil {
				require.NoError(err)
				require.Equal(test.expected, actual)
			} else {
				require.Error(err)
				require.ErrorContains(err, test.expectedErr.Error())
			}
		})
	}
}

func TestVersionType_UnmarshalText(t *testing.T) {
	t.Parallel()

	testCases := []struct { //nolint:govet // tests
		name        string
		value       []byte
		expected    utilsTLS.VersionType
		expectedErr error
	}{
		{
			name:     "TLS13",
			value:    []byte("TLS13"),
			expected: utilsTLS.VersionType(tls.VersionTLS13),
		},
		{
			name:     "TLS12",
			value:    []byte("TLS12"),
			expected: utilsTLS.VersionType(tls.VersionTLS12),
		},
		{
			name:     "TLS11",
			value:    []byte("TLS11"),
			expected: utilsTLS.VersionType(tls.VersionTLS11),
		},
		{
			name:     "TLS10",
			value:    []byte("TLS10"),
			expected: utilsTLS.VersionType(tls.VersionTLS10),
		},
		{
			name:        "Unknown",
			value:       []byte("Unknown"),
			expectedErr: errors.New("unknown TLS version"),
		},
	}

	for i := range testCases {
		test := testCases[i]

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			require := require.New(t)

			var actual utilsTLS.VersionType

			err := actual.UnmarshalText(test.value)
			if test.expectedErr == nil {
				require.NoError(err)
				require.Equal(test.expected, actual)
			} else {
				require.Error(err)
				require.ErrorContains(err, test.expectedErr.Error())
			}
		})
	}
}
