package tls

import (
	"crypto/tls"
	"fmt"
	"strings"
)

type (
	ClientAuthType tls.ClientAuthType
	VersionType    uint16
)

func (p *ClientAuthType) UnmarshalText(text []byte) error {
	var v ClientAuthType

	switch string(text) {
	case "RequestClientCert":
		v = ClientAuthType(tls.RequestClientCert)
	case "RequireClientCert":
		v = ClientAuthType(tls.RequireAnyClientCert)
	case "VerifyClientCertIfGiven":
		v = ClientAuthType(tls.VerifyClientCertIfGiven)
	case "RequireAndVerifyClientCert":
		v = ClientAuthType(tls.RequireAndVerifyClientCert)
	case "NoClientCert":
		v = ClientAuthType(tls.NoClientCert)
	default:
		switch strings.ToLower(string(text)) {
		case "request_client_cert":
			v = ClientAuthType(tls.RequestClientCert)
		case "require_client_cert":
			v = ClientAuthType(tls.RequireAnyClientCert)
		case "verify_client_cert_if_given":
			v = ClientAuthType(tls.VerifyClientCertIfGiven)
		case "require_and_verify_client_cert":
			v = ClientAuthType(tls.RequireAndVerifyClientCert)
		case "no_client_cert":
			v = ClientAuthType(tls.NoClientCert)
		default:
			return fmt.Errorf("unknown ClientAuthType: %s", string(text))
		}
	}

	*p = v

	return nil
}

func (p *VersionType) UnmarshalText(text []byte) error {
	var v VersionType

	switch strings.ToUpper(string(text)) {
	case "TLS13":
		v = VersionType(tls.VersionTLS13)
	case "TLS12":
		v = VersionType(tls.VersionTLS12)
	case "TLS11":
		v = VersionType(tls.VersionTLS11)
	case "TLS10":
		v = VersionType(tls.VersionTLS10)
	default:
		return fmt.Errorf("unknown TLS version: %s", string(text))
	}

	*p = v

	return nil
}
