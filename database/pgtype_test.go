package database_test

import (
	"encoding/json"
	"testing"

	"github.com/jackc/pgx/v5/pgtype"
	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-utils/database"
)

func BenchmarkJsonEncodeValue(b *testing.B) {
	value := database.JSON{
		"glossary": map[string]any{
			"GlossDiv": map[string]any{
				"GlossList": map[string]any{
					"GlossEntry": map[string]any{
						"Abbrev":  "ISO 8879:1986",
						"Acronym": "SGML",
						"GlossDef": map[string]any{
							"GlossSeeAlso": []any{"GML", "XML"},
							"para":         "A meta-markup language, used to create markup languages such as DocBook.",
						},
						"GlossSee":  "markup",
						"GlossTerm": "Standard Generalized Markup Language",
						"ID":        "SGML",
						"SortAs":    "SGML",
					},
				},
				"title": "S",
			},
			"title": "example glossary",
		},
	}

	typeMap := pgtype.NewMap()
	typeMap.RegisterType(&pgtype.Type{
		Name:  "old json",
		OID:   1,
		Codec: &pgtype.JSONCodec{Marshal: json.Marshal},
	})
	typeMap.RegisterType(&pgtype.Type{
		Name:  "new json",
		OID:   2,
		Codec: &database.JSONCodec{},
	})

	b.Run("Old", func(b *testing.B) {
		var err error

		buf := make([]byte, 0, 2048)
		plan := typeMap.PlanEncode(1, pgtype.TextFormatCode, &value)

		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			buf, err = plan.Encode(&value, buf)
			require.NoError(b, err)

			buf = buf[:0]
		}
	})
	b.Run("New", func(b *testing.B) {
		var err error

		buf := make([]byte, 0, 2048)
		plan := typeMap.PlanEncode(2, pgtype.TextFormatCode, &value)

		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			buf, err = plan.Encode(&value, buf)
			require.NoError(b, err)

			buf = buf[:0]
		}
	})
}

func BenchmarkJsonScanValue(b *testing.B) {
	//nolint:lll // test data
	data := []byte(`{"glossary":{"GlossDiv":{"GlossList":{"GlossEntry":{"Abbrev":"ISO 8879:1986","Acronym":"SGML","GlossDef":{"GlossSeeAlso":["GML","XML"],"para":"A meta-markup language, used to create markup languages such as DocBook."},"GlossSee":"markup","GlossTerm":"Standard Generalized Markup Language","ID":"SGML","SortAs":"SGML"}},"title":"S"},"title":"example glossary"}}`)

	typeMap := pgtype.NewMap()
	typeMap.RegisterType(&pgtype.Type{
		Name:  "old json",
		OID:   1,
		Codec: &pgtype.JSONCodec{Unmarshal: json.Unmarshal},
	})
	typeMap.RegisterType(&pgtype.Type{
		Name:  "new json",
		OID:   2,
		Codec: &database.JSONCodec{Unmarshal: json.Unmarshal},
	})

	b.Run("Old", func(b *testing.B) {
		value := database.JSON{}
		plan := typeMap.PlanScan(1, pgtype.TextFormatCode, &value)

		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			plan.Scan(data, &value) //nolint:errcheck // benchmark
		}
	})
	b.Run("New", func(b *testing.B) {
		json := database.JSON{}
		plan := typeMap.PlanScan(2, pgtype.TextFormatCode, &json)

		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			plan.Scan(data, &json) //nolint:errcheck // benchmark
		}
	})
}
