package database

import (
	"database/sql/driver"

	"github.com/jackc/pgx/v5/pgtype"
	"gitlab.com/dwagin/go-json"
)

type JSONCodec struct {
	Unmarshal func(data []byte, v any) error
}

func (*JSONCodec) FormatSupported(format int16) bool {
	return format == pgtype.TextFormatCode || format == pgtype.BinaryFormatCode
}

func (*JSONCodec) PreferredFormat() int16 {
	return pgtype.TextFormatCode
}

func (c *JSONCodec) PlanEncode(m *pgtype.Map, oid uint32, format int16, value any) pgtype.EncodePlan {
	switch value.(type) {
	case string:
		return encodePlanJSONCodecEitherFormatString{}
	case []byte:
		return encodePlanJSONCodecEitherFormatByteSlice{}
	}

	// Because anything can be marshalled the normal wrapping in Map.PlanScan doesn't get a chance to run. So try the
	// appropriate wrappers here.
	for _, f := range []pgtype.TryWrapEncodePlanFunc{
		pgtype.TryWrapDerefPointerEncodePlan,
		pgtype.TryWrapFindUnderlyingTypeEncodePlan,
	} {
		if wrapperPlan, nextValue, ok := f(value); ok {
			if nextPlan := c.PlanEncode(m, oid, format, nextValue); nextPlan != nil {
				wrapperPlan.SetNext(nextPlan)

				return wrapperPlan
			}
		}
	}

	return encodePlanJSONCodec{}
}

type encodePlanJSONCodecEitherFormatString struct{}

func (encodePlanJSONCodecEitherFormatString) Encode(value any, buf []byte) ([]byte, error) {
	jsonString := value.(string) //nolint:errcheck,forcetypeassert // already checked

	return append(buf, jsonString...), nil
}

type encodePlanJSONCodecEitherFormatByteSlice struct{}

func (encodePlanJSONCodecEitherFormatByteSlice) Encode(value any, buf []byte) ([]byte, error) {
	jsonBytes := value.([]byte) //nolint:errcheck,forcetypeassert // already checked
	if jsonBytes == nil {
		return nil, nil
	}

	return append(buf, jsonBytes...), nil
}

type encodePlanJSONCodec struct{}

func (encodePlanJSONCodec) Encode(value any, buf []byte) ([]byte, error) {
	return json.Append(buf, value), nil
}

func (c *JSONCodec) PlanScan(m *pgtype.Map, oid uint32, format int16, target any) pgtype.ScanPlan {
	if _, ok := target.(*JSON); ok {
		return scanPlanJSONCodec{}
	}

	return (&pgtype.JSONCodec{Unmarshal: c.Unmarshal}).PlanScan(m, oid, format, target)
}

type scanPlanJSONCodec struct{}

func (scanPlanJSONCodec) Scan(src []byte, dst any) error {
	value := dst.(*JSON) //nolint:errcheck,forcetypeassert // already checked

	if src == nil {
		*value = nil
	}

	return value.UnmarshalJSON(src)
}

func (c *JSONCodec) DecodeDatabaseSQLValue(m *pgtype.Map, oid uint32, format int16, src []byte) (driver.Value, error) {
	//nolint:wrapcheck // transparent wrapper
	return (&pgtype.JSONCodec{Unmarshal: c.Unmarshal}).DecodeDatabaseSQLValue(m, oid, format, src)
}

func (c *JSONCodec) DecodeValue(_ *pgtype.Map, _ uint32, _ int16, src []byte) (any, error) {
	if src == nil {
		return nil, nil //nolint:nilnil // valid value
	}

	var dst JSON
	if err := dst.UnmarshalJSON(src); err != nil {
		return nil, err
	}

	return dst, nil
}
