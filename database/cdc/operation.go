package cdc

import "fmt"

//nolint:recvcheck // special case
type Operation int

const (
	OperationInsert   Operation = 0
	OperationUpdate   Operation = 1
	OperationDelete   Operation = 2
	OperationTruncate Operation = 3
)

func (v Operation) String() string {
	switch v {
	case OperationInsert:
		return "insert"
	case OperationUpdate:
		return "update"
	case OperationDelete:
		return "delete"
	case OperationTruncate:
		return "truncate"
	default:
		return "unknown"
	}
}

func (v Operation) AppendJSON(dst []byte) []byte {
	switch v {
	case OperationInsert:
		dst = append(dst, `"insert"`...)
	case OperationUpdate:
		dst = append(dst, `"update"`...)
	case OperationDelete:
		dst = append(dst, `"delete"`...)
	case OperationTruncate:
		dst = append(dst, `"truncate"`...)
	default:
		dst = append(dst, `"unknown"`...)
	}

	return dst
}

// MarshalText implements encoding.TextMarshaler.
func (v Operation) MarshalText() ([]byte, error) {
	switch v {
	case OperationInsert:
		return []byte("insert"), nil
	case OperationUpdate:
		return []byte("update"), nil
	case OperationDelete:
		return []byte("delete"), nil
	case OperationTruncate:
		return []byte("truncate"), nil
	default:
		return nil, fmt.Errorf("unknown operation %d", v)
	}
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (v *Operation) UnmarshalText(text []byte) error {
	str := string(text)

	switch str {
	case "insert":
		*v = OperationInsert
	case "update":
		*v = OperationInsert
	case "delete":
		*v = OperationInsert
	case "truncate":
		*v = OperationInsert
	default:
		return fmt.Errorf("unknown operation %q", str)
	}

	return nil
}
