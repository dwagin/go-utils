package cdc

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"gitlab.com/dwagin/go-json"

	"gitlab.com/dwagin/go-utils/database"
)

//nolint:govet // protocol structure
type Message struct {
	Operation Operation `cbor:"1,keyasint" json:"operation,string"`

	Schema string `cbor:"2,keyasint" json:"schema"`
	Table  string `cbor:"3,keyasint" json:"table"`

	Before *Tuple `cbor:"4,keyasint,omitempty" json:"before,omitempty"`
	After  *Tuple `cbor:"5,keyasint,omitempty" json:"after,omitempty"`

	PK *Columns `cbor:"6,keyasint,omitempty" json:"pk,omitempty"`

	CommitTime time.Time `cbor:"7,keyasint" json:"commit_time"`
}

type Key []any

var messagePool sync.Pool

func AcquireMessage() *Message {
	if item := messagePool.Get(); item != nil {
		return item.(*Message) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(Message)
}

func (v *Message) String() string {
	if v == nil {
		return Nil
	}

	dst := make([]byte, 0, 2048)

	dst = append(dst, `{op=`...)
	dst = append(dst, v.Operation.String()...)

	dst = append(dst, ` schema=`...)
	dst = append(dst, database.Sanitize(v.Schema)...)

	dst = append(dst, ` table=`...)
	dst = append(dst, database.Sanitize(v.Table)...)

	if v.Before != nil {
		dst = append(dst, ` before=`...)
		dst = v.Before.AppendJSON(dst)
	}

	if v.After != nil {
		dst = append(dst, ` after=`...)
		dst = v.After.AppendJSON(dst)
	}

	if v.PK != nil {
		dst = append(dst, ` pk=`...)
		dst = v.PK.AppendJSON(dst)
	}

	dst = append(dst, ` commit_time=`...)
	dst = v.CommitTime.UTC().AppendFormat(dst, time.RFC3339Nano)

	dst = append(dst, '}')

	return string(dst)
}

func (v *Message) AppendJSON(dst []byte) []byte {
	if v == nil {
		return append(dst, json.Null...)
	}

	dst = append(dst, `{"op":`...)
	dst = v.Operation.AppendJSON(dst)

	dst = append(dst, `,"schema":`...)
	dst = json.AppendString(dst, v.Schema)

	dst = append(dst, `,"table":`...)
	dst = json.AppendString(dst, v.Table)

	if v.Before != nil {
		dst = append(dst, `,"before":`...)
		dst = v.Before.AppendJSON(dst)
	}

	if v.After != nil {
		dst = append(dst, `,"after":`...)
		dst = v.After.AppendJSON(dst)
	}

	if v.PK != nil {
		dst = append(dst, `,"pk":`...)
		dst = v.PK.AppendJSON(dst)
	}

	dst = append(dst, `,"commit_time":`...)
	dst = json.AppendTime(dst, v.CommitTime)

	return append(dst, '}')
}

func (v *Message) Validate() error {
	switch v.Operation {
	case OperationInsert:
		if v.Before != nil {
			return errors.New("unexpected before tuple")
		}

		if v.After == nil {
			return errors.New("after tuple not defined")
		}
	case OperationUpdate:
		if v.Before == nil && (v.PK == nil || len(*v.PK) == 0) {
			return errors.New("before tuple not defined")
		}

		if v.After == nil {
			return errors.New("after tuple not defined")
		}
	case OperationDelete:
		if v.Before == nil {
			return errors.New("before tuple not defined")
		}

		if v.After != nil {
			return errors.New("unexpected after tuple")
		}
	case OperationTruncate:
	default:
		return errors.New("unknown operation")
	}

	if v.Schema == "" {
		return errors.New("empty schema name")
	}

	if v.Table == "" {
		return errors.New("empty table name")
	}

	return nil
}

func (v *Message) Key(redefine Columns) (Key, error) {
	var (
		columns Columns
		tuple   Tuple
	)

	switch {
	case len(redefine) > 0:
		columns = redefine
	case v.PK != nil && len(*v.PK) > 0:
		columns = *v.PK
	default:
		return nil, ErrKeyNotDefined
	}

	switch {
	case v.After != nil:
		tuple = *v.After
	case v.Before != nil:
		tuple = *v.Before
	default:
		return nil, ErrKeyNotDefined
	}

	key := make(Key, 0, len(columns))

	for _, column := range columns {
		if val, ok := tuple[column]; ok {
			key = append(key, val)
		} else {
			return nil, fmt.Errorf("key column %q not found", column)
		}
	}

	return key, nil
}

func (v *Message) Release() {
	if v == nil {
		panic("Message: not defined")
	}

	if v.Before != nil {
		v.Before.Release()
		v.Before = nil
	}

	if v.After != nil {
		v.After.Release()
		v.After = nil
	}

	if v.PK != nil {
		v.PK.Release()
		v.PK = nil
	}

	*v = Message{}

	messagePool.Put(v)
}
