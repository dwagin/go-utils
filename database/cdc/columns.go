package cdc

import (
	"sync"

	"gitlab.com/dwagin/go-json"
)

type Columns []string

var columnsPool sync.Pool

func AcquireColumns(size int) *Columns {
	if item := columnsPool.Get(); item != nil {
		columns := item.(*Columns) //nolint:errcheck,forcetypeassert // redundant
		if cap(*columns) >= size {
			return columns
		}
	}

	columns := make(Columns, 0, size)

	return &columns
}

func (v *Columns) String() string {
	if v == nil || *v == nil {
		return Nil
	}

	if len(*v) == 0 {
		return EmptySlice
	}

	dst := v.AppendJSON(make([]byte, 0, 1024))

	return string(dst)
}

func (v *Columns) AppendJSON(dst []byte) []byte {
	if v == nil {
		return append(dst, json.Null...)
	}

	return json.AppendSliceString(dst, *v)
}

func (v *Columns) Release() {
	if v == nil {
		panic("Columns: not defined")
	}

	clear(*v)

	*v = (*v)[:0]

	columnsPool.Put(v)
}
