package cdc

import (
	"fmt"
	"sync"

	"gitlab.com/dwagin/go-json"
)

type Tuple map[string]any

var tuplePool sync.Pool

func AcquireTuple(size int) *Tuple {
	if item := tuplePool.Get(); item != nil {
		return item.(*Tuple) //nolint:errcheck,forcetypeassert // redundant
	}

	item := make(Tuple, size)

	return &item
}

func NewTuple(columns Columns, row []any) (*Tuple, error) {
	if len(columns) != len(row) {
		return nil, fmt.Errorf("wrong number of columns %d != %d", len(columns), len(row))
	}

	tuple := AcquireTuple(len(columns))
	m := *tuple

	for i := range columns {
		m[columns[i]] = row[i]
	}

	return tuple, nil
}

func (v *Tuple) String() string {
	if v == nil || *v == nil {
		return Nil
	}

	if len(*v) == 0 {
		return EmptyMap
	}

	dst := v.AppendJSON(make([]byte, 0, 1024))

	return string(dst)
}

func (v *Tuple) AppendJSON(dst []byte) []byte {
	if v == nil {
		return append(dst, json.Null...)
	}

	return json.AppendMap(dst, *v)
}

func (v *Tuple) Release() {
	if v == nil {
		panic("Tuple: not defined")
	}

	clear(*v)

	tuplePool.Put(v)
}

func TupleValue[T any](tuple Tuple, key string) (*T, error) {
	var (
		ok  bool
		val any
		ret T
	)

	if val, ok = tuple[key]; !ok {
		return nil, newTupleErrorf("tuple does not contain the %q key", key)
	}

	if val == nil {
		return nil, newTupleErrorf("tuple value for %q key error: %w", key, ErrUnexpectedNULL)
	}

	if ret, ok = val.(T); !ok {
		return nil, newTupleErrorf("tuple contains key %q of the wrong type %T", key, val)
	}

	return &ret, nil
}
