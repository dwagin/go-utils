package cdc_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-utils/database/cdc"
)

type TupleValueSuite struct {
	suite.Suite
}

var testTuple = cdc.Tuple{
	"key0":  byte('0'),
	"key1":  []byte("value1"),
	"key2":  "value2",
	"key3":  int8(-127),
	"key4":  int16(32767),
	"key5":  int32(2147483647),
	"key6":  int64(-9223372036854775808),
	"key7":  true,
	"key8":  float32(942.392),
	"key9":  float64(-31283.45345392),
	"key10": nil,
}

func TestTupleValue(t *testing.T) {
	suite.Run(t, new(TupleValueSuite))
}

func (v *TupleValueSuite) TestByte() {
	require := v.Require()
	value, err := cdc.TupleValue[byte](testTuple, "key0")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(byte('0'), *value)
}

func (v *TupleValueSuite) TestByteSlice() {
	require := v.Require()
	value, err := cdc.TupleValue[[]byte](testTuple, "key1")
	require.NoError(err)
	require.NotNil(value)
	require.Equal([]byte("value1"), *value)
}

func (v *TupleValueSuite) TestString() {
	require := v.Require()
	value, err := cdc.TupleValue[string](testTuple, "key2")
	require.NoError(err)
	require.NotNil(value)
	require.Equal("value2", *value)
}

func (v *TupleValueSuite) TestInt8() {
	require := v.Require()
	value, err := cdc.TupleValue[int8](testTuple, "key3")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int8(-127), *value)
}

func (v *TupleValueSuite) TestInt16() {
	require := v.Require()
	value, err := cdc.TupleValue[int16](testTuple, "key4")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int16(32767), *value)
}

func (v *TupleValueSuite) TestInt32() {
	require := v.Require()
	value, err := cdc.TupleValue[int32](testTuple, "key5")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int32(2147483647), *value)
}

func (v *TupleValueSuite) TestInt64() {
	require := v.Require()
	value, err := cdc.TupleValue[int64](testTuple, "key6")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int64(-9223372036854775808), *value)
}

func (v *TupleValueSuite) TestBool() {
	require := v.Require()
	value, err := cdc.TupleValue[bool](testTuple, "key7")
	require.NoError(err)
	require.NotNil(value)
	require.True(*value)
}

func (v *TupleValueSuite) TestFloat32() {
	require := v.Require()
	value, err := cdc.TupleValue[float32](testTuple, "key8")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(float32(942.392), *value) //nolint:testifylint // must be same value
}

func (v *TupleValueSuite) TestFloat64() {
	require := v.Require()
	value, err := cdc.TupleValue[float64](testTuple, "key9")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(float64(-31283.45345392), *value) //nolint:testifylint // must be same value
}

func (v *TupleValueSuite) TestNonExistingKey() {
	var tupleErr *cdc.TupleError

	require := v.Require()
	value, err := cdc.TupleValue[int](testTuple, "non-existing-key")
	require.Error(err)
	require.ErrorAs(err, &tupleErr)
	require.ErrorContains(err, "not contain")
	require.Nil(value)
}

func (v *TupleValueSuite) TestNilValue() {
	var tupleErr *cdc.TupleError

	require := v.Require()
	value, err := cdc.TupleValue[int](testTuple, "key10")
	require.Error(err)
	require.ErrorAs(err, &tupleErr)
	require.ErrorContains(err, "unexpected NULL")
	require.ErrorIs(err, cdc.ErrUnexpectedNULL)
	require.Nil(value)
}

func (v *TupleValueSuite) TestWrongTypeErr() {
	var tupleErr *cdc.TupleError

	require := v.Require()
	value, err := cdc.TupleValue[bool](testTuple, "key0")
	require.Error(err)
	require.ErrorAs(err, &tupleErr)
	require.ErrorContains(err, "wrong type")
	require.Nil(value)
}

func BenchmarkTuple_Allocation(b *testing.B) {
	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			tuple := make(cdc.Tuple, len(testTuple))
			for key, value := range testTuple {
				tuple[key] = value
			}
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				tuple := make(cdc.Tuple, len(testTuple))
				for key, value := range testTuple {
					tuple[key] = value
				}
			}
		})
	})
}

func BenchmarkTuple_SyncPool(b *testing.B) {
	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			tuple := cdc.AcquireTuple(len(testTuple))
			m := *tuple

			for key, value := range testTuple {
				m[key] = value
			}

			tuple.Release()
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				tuple := cdc.AcquireTuple(len(testTuple))
				m := *tuple

				for key, value := range testTuple {
					m[key] = value
				}

				tuple.Release()
			}
		})
	})
}
