package cdc

const (
	EmptyMap   = "{}"
	EmptySlice = "[]"
	Nil        = "nil"
)
