package cdc

import (
	"reflect"

	"github.com/fxamacker/cbor/v2"
	"github.com/jackc/pgx/v5/pgtype"

	utilsDatabase "gitlab.com/dwagin/go-utils/database"
)

var (
	encMode cbor.EncMode
	decMode cbor.DecMode
)

//nolint:gochecknoinits // init cbor
func init() {
	var err error

	tags := cbor.NewTagSet()

	err = tags.Add(
		cbor.TagOptions{EncTag: cbor.EncTagRequired, DecTag: cbor.DecTagRequired},
		reflect.TypeOf(utilsDatabase.JSON{}),
		999,
	)
	if err != nil {
		panic(err)
	}

	err = tags.Add(
		cbor.TagOptions{EncTag: cbor.EncTagRequired, DecTag: cbor.DecTagRequired},
		reflect.TypeOf(pgtype.Numeric{}),
		998,
	)
	if err != nil {
		panic(err)
	}

	encOpts := cbor.EncOptions{
		Time:        cbor.TimeUnixMicro,
		TimeTag:     cbor.EncTagRequired,
		IndefLength: cbor.IndefLengthForbidden,
	}

	encMode, err = encOpts.EncModeWithTags(tags)
	if err != nil {
		panic(err)
	}

	decOpts := cbor.DecOptions{
		TimeTag:          cbor.DecTagRequired,
		MaxNestedLevels:  128,
		MaxArrayElements: 2048,
		MaxMapPairs:      2048,
		IndefLength:      cbor.IndefLengthForbidden,
		IntDec:           cbor.IntDecConvertSignedOrFail,
	}

	decMode, err = decOpts.DecModeWithTags(tags)
	if err != nil {
		panic(err)
	}
}

func Marshal(data any) ([]byte, error) {
	return encMode.Marshal(data) //nolint:wrapcheck // transparent wrapper
}

func Unmarshal(raw []byte, data any) error {
	return decMode.Unmarshal(raw, data) //nolint:wrapcheck // transparent wrapper
}
