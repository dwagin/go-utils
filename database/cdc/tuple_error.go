package cdc

import (
	"errors"
	"fmt"
)

type TupleError struct {
	err error
}

func (e *TupleError) Error() string {
	return e.err.Error()
}

func (e *TupleError) Unwrap() error {
	return e.err
}

func newTupleError(text string) error {
	return &TupleError{err: errors.New(text)}
}

func newTupleErrorf(format string, a ...any) error {
	return &TupleError{err: fmt.Errorf(format, a...)}
}

var (
	ErrKeyNotDefined  = newTupleError("key not defined")
	ErrUnexpectedNULL = newTupleError("unexpected NULL")
)
