package cdc_test

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-utils/database/cdc"
)

//nolint:funlen // tests
func TestMessage_Validate(t *testing.T) {
	t.Parallel()

	testCases := []struct { //nolint:govet // tests
		name     string
		value    cdc.Message
		expected error
	}{
		{
			name: "valid insert operation",
			value: cdc.Message{
				Operation:  cdc.OperationInsert,
				Schema:     "test_schema",
				Table:      "test_table",
				After:      &cdc.Tuple{"column": "value"},
				CommitTime: time.Now(),
			},
		},
		{
			name: "invalid insert operation with before tuple",
			value: cdc.Message{
				Operation:  cdc.OperationInsert,
				Schema:     "test_schema",
				Table:      "test_table",
				Before:     &cdc.Tuple{"column": "value"},
				After:      &cdc.Tuple{"column": "value"},
				CommitTime: time.Now(),
			},
			expected: errors.New("unexpected before tuple"),
		},
		{
			name: "valid update operation",
			value: cdc.Message{
				Operation:  cdc.OperationUpdate,
				Schema:     "test_schema",
				Table:      "test_table",
				Before:     &cdc.Tuple{"column": "old_value"},
				After:      &cdc.Tuple{"column": "new_value"},
				CommitTime: time.Now(),
			},
		},
		{
			name: "invalid update operation with empty before and nil PK",
			value: cdc.Message{
				Operation:  cdc.OperationUpdate,
				Schema:     "test_schema",
				Table:      "test_table",
				After:      &cdc.Tuple{"column": "new_value"},
				CommitTime: time.Now(),
			},
			expected: errors.New("before tuple not defined"),
		},
		{
			name: "valid delete operation",
			value: cdc.Message{
				Operation:  cdc.OperationDelete,
				Schema:     "test_schema",
				Table:      "test_table",
				Before:     &cdc.Tuple{"column": "value"},
				CommitTime: time.Now(),
			},
		},
		{
			name: "invalid delete operation with after tuple",
			value: cdc.Message{
				Operation:  cdc.OperationDelete,
				Schema:     "test_schema",
				Table:      "test_table",
				Before:     &cdc.Tuple{"column": "value"},
				After:      &cdc.Tuple{"column": "unexpected_value"},
				CommitTime: time.Now(),
			},
			expected: errors.New("unexpected after tuple"),
		},
		{
			name: "valid truncate operation",
			value: cdc.Message{
				Operation:  cdc.OperationTruncate,
				Schema:     "test_schema",
				Table:      "test_table",
				CommitTime: time.Now(),
			},
		},
		{
			name: "empty schema name",
			value: cdc.Message{
				Operation:  cdc.OperationTruncate,
				Table:      "test_table",
				CommitTime: time.Now(),
			},
			expected: errors.New("empty schema name"),
		},
		{
			name: "empty table name",
			value: cdc.Message{
				Operation:  cdc.OperationTruncate,
				Schema:     "test_schema",
				CommitTime: time.Now(),
			},
			expected: errors.New("empty table name"),
		},
		{
			name: "unknown operation",
			value: cdc.Message{
				Operation:  cdc.Operation(-1),
				Schema:     "test_schema",
				Table:      "test_table",
				CommitTime: time.Now(),
			},
			expected: errors.New("unknown operation"),
		},
	}

	for i := range testCases {
		test := testCases[i]

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			require := require.New(t)

			err := test.value.Validate()
			if test.expected == nil {
				require.NoError(err)
			} else {
				require.Error(err)
				require.ErrorContains(err, test.expected.Error())
			}
		})
	}
}

func BenchmarkMessage_Allocation(b *testing.B) {
	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			//nolint:govet // unused write
			msg := &cdc.Message{
				Schema:     "namespace",
				Table:      "name",
				Operation:  cdc.OperationUpdate,
				CommitTime: time.Now(),
			}

			tuple := make(cdc.Tuple, len(testTuple))
			for key, value := range testTuple {
				tuple[key] = value
			}

			msg.Before = &tuple //nolint:govet // unused write

			tuple = make(cdc.Tuple, len(testTuple))
			for key, value := range testTuple {
				tuple[key] = value
			}

			msg.After = &tuple //nolint:govet // unused write
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				//nolint:govet // unused write
				msg := &cdc.Message{
					Schema:     "namespace",
					Table:      "name",
					Operation:  cdc.OperationUpdate,
					CommitTime: time.Now(),
				}

				tuple := make(cdc.Tuple, len(testTuple))
				for key, value := range testTuple {
					tuple[key] = value
				}

				msg.Before = &tuple //nolint:govet // unused write

				tuple = make(cdc.Tuple, len(testTuple))
				for key, value := range testTuple {
					tuple[key] = value
				}

				msg.After = &tuple //nolint:govet // unused write
			}
		})
	})
}

func BenchmarkMessage_SyncPool(b *testing.B) {
	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			msg := cdc.AcquireMessage()

			msg.Schema = "namespace"
			msg.Table = "name"
			msg.Operation = cdc.OperationUpdate
			msg.CommitTime = time.Now()

			msg.Before = cdc.AcquireTuple(len(testTuple))
			tuple := *msg.Before

			for key, value := range testTuple {
				tuple[key] = value
			}

			msg.After = cdc.AcquireTuple(len(testTuple))
			tuple = *msg.After

			for key, value := range testTuple {
				tuple[key] = value
			}

			msg.Release()
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				msg := cdc.AcquireMessage()

				msg.Schema = "namespace"
				msg.Table = "name"
				msg.Operation = cdc.OperationUpdate
				msg.CommitTime = time.Now()

				msg.Before = cdc.AcquireTuple(len(testTuple))
				tuple := *msg.Before

				for key, value := range testTuple {
					tuple[key] = value
				}

				msg.After = cdc.AcquireTuple(len(testTuple))
				tuple = *msg.After

				for key, value := range testTuple {
					tuple[key] = value
				}

				msg.Release()
			}
		})
	})
}
