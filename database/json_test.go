package database_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/dwagin/go-utils/database"
)

type JSONValueSuite struct {
	suite.Suite
}

var testJSON = database.JSON{
	"key0":  byte('0'),
	"key1":  []byte("value1"),
	"key2":  "value2",
	"key3":  int8(-127),
	"key4":  int16(32767),
	"key5":  int32(2147483647),
	"key6":  int64(-9223372036854775808),
	"key7":  true,
	"key8":  float32(942.392),
	"key9":  float64(-31283.45345392),
	"key10": nil,
}

func TestJSONValue(t *testing.T) {
	suite.Run(t, new(JSONValueSuite))
}

func (v *JSONValueSuite) TestByte() {
	require := v.Require()
	value, err := database.JSONValue[byte](testJSON, "key0")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(byte('0'), *value)
}

func (v *JSONValueSuite) TestByteSlice() {
	require := v.Require()
	value, err := database.JSONValue[[]byte](testJSON, "key1")
	require.NoError(err)
	require.NotNil(value)
	require.Equal([]byte("value1"), *value)
}

func (v *JSONValueSuite) TestString() {
	require := v.Require()
	value, err := database.JSONValue[string](testJSON, "key2")
	require.NoError(err)
	require.NotNil(value)
	require.Equal("value2", *value)
}

func (v *JSONValueSuite) TestInt8() {
	require := v.Require()
	value, err := database.JSONValue[int8](testJSON, "key3")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int8(-127), *value)
}

func (v *JSONValueSuite) TestInt16() {
	require := v.Require()
	value, err := database.JSONValue[int16](testJSON, "key4")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int16(32767), *value)
}

func (v *JSONValueSuite) TestInt32() {
	require := v.Require()
	value, err := database.JSONValue[int32](testJSON, "key5")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int32(2147483647), *value)
}

func (v *JSONValueSuite) TestInt64() {
	require := v.Require()
	value, err := database.JSONValue[int64](testJSON, "key6")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(int64(-9223372036854775808), *value)
}

func (v *JSONValueSuite) TestBoolean() {
	require := v.Require()
	value, err := database.JSONValue[bool](testJSON, "key7")
	require.NoError(err)
	require.NotNil(value)
	require.True(*value)
}

func (v *JSONValueSuite) TestFloat32() {
	require := v.Require()
	value, err := database.JSONValue[float32](testJSON, "key8")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(float32(942.392), *value) //nolint:testifylint // must be same value
}

func (v *JSONValueSuite) TestFloat64() {
	require := v.Require()
	value, err := database.JSONValue[float64](testJSON, "key9")
	require.NoError(err)
	require.NotNil(value)
	require.Equal(float64(-31283.45345392), *value) //nolint:testifylint // must be same value
}

func (v *JSONValueSuite) TestNonExistingKey() {
	var jsonErr *database.JSONError

	require := v.Require()
	value, err := database.JSONValue[int](testJSON, "non-existing-key")
	require.Error(err)
	require.ErrorAs(err, &jsonErr)
	require.ErrorContains(err, "not contain")
	require.ErrorContains(err, "unexpected null")
	require.ErrorIs(err, database.ErrUnexpectedNULL)
	require.Nil(value)
}

func (v *JSONValueSuite) TestNilValue() {
	var jsonErr *database.JSONError

	require := v.Require()
	value, err := database.JSONValue[int](testJSON, "key10")
	require.Error(err)
	require.ErrorAs(err, &jsonErr)
	require.ErrorContains(err, "unexpected null")
	require.ErrorIs(err, database.ErrUnexpectedNULL)
	require.Nil(value)
}

func (v *JSONValueSuite) TestWrongTypeErr() {
	var jsonErr *database.JSONError

	require := v.Require()
	value, err := database.JSONValue[bool](testJSON, "key0")
	require.Error(err)
	require.ErrorAs(err, &jsonErr)
	require.ErrorContains(err, "wrong type")
	require.Nil(value)
}
