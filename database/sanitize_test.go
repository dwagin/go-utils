package database_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-utils/database"
)

func TestSanitize(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		value    string
		expected string
	}{
		{
			name:     "Simple",
			value:    `column1`,
			expected: `"column1"`,
		},
		{
			name:     "Contain 0 byte",
			value:    `col` + string([]byte{0}) + `umn1`,
			expected: `"column1"`,
		},
		{
			name:     "Contain \" symbol",
			value:    `"column1"`,
			expected: `"""column1"""`,
		},
	}

	for i := range testCases {
		test := testCases[i]

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			actual := database.Sanitize(test.value)
			require.Equal(t, test.expected, actual)
		})
	}
}

func SanitizeOld(ident string) string {
	s := strings.ReplaceAll(ident, string([]byte{0}), "")

	return `"` + strings.ReplaceAll(s, `"`, `""`) + `"`
}

func BenchmarkSanitize(b *testing.B) {
	testCases := []struct {
		name  string
		value string
	}{
		{
			name:  "Simple",
			value: `column1`,
		},
		{
			name:  "Contain 0 byte",
			value: `col` + string([]byte{0}) + `umn1`,
		},
		{
			name:  "Contain \" symbol",
			value: `"column1"`,
		},
	}

	b.Run("Old", func(b *testing.B) {
		for i := range testCases {
			test := testCases[i]
			b.Run(test.name, func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					_ = SanitizeOld(test.value)
				}
			})
		}
	})
	b.Run("New", func(b *testing.B) {
		for i := range testCases {
			test := testCases[i]
			b.Run(test.name, func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					_ = database.Sanitize(test.value)
				}
			})
		}
	})
}
