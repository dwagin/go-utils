package database

import (
	_ "github.com/mailru/easyjson/gen" // easyjson generator
	"gitlab.com/dwagin/go-json"
)

//go:generate easyjson $GOFILE

//easyjson:json
//nolint:recvcheck // special case
type JSON map[string]any

func (v JSON) AppendJSON(dst []byte) []byte {
	return json.AppendMap(dst, v)
}

func JSONValue[T any](json JSON, key string) (*T, error) {
	var (
		ok  bool
		val any
		ret T
	)

	if val, ok = json[key]; !ok {
		return nil, newJSONErrorf("json does not contain the %q key: %w", key, ErrUnexpectedNULL)
	}

	if val == nil {
		return nil, newJSONErrorf("json value for %q key error: %w", key, ErrUnexpectedNULL)
	}

	if ret, ok = val.(T); !ok {
		return nil, newJSONErrorf("json contains key %q of the wrong type %T", key, val)
	}

	return &ret, nil
}
