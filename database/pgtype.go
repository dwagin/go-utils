package database

import (
	"errors"

	"github.com/jackc/pgx/v5/pgtype"
)

func codecScan(codec pgtype.Codec, m *pgtype.Map, oid uint32, format int16, src []byte, dst any) error {
	scanPlan := codec.PlanScan(m, oid, format, dst)
	if scanPlan == nil {
		return errors.New("PlanScan did not find a plan")
	}

	return scanPlan.Scan(src, dst) //nolint:wrapcheck // transparent wrapper
}
