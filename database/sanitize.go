package database

import "unicode/utf8"

func Sanitize(ident string) string {
	buf := make([]byte, 0, 512)

	buf = append(buf, '"')

	for _, r := range ident {
		switch r {
		case 0x0:
		case '"':
			buf = append(buf, `""`...)
		default:
			buf = utf8.AppendRune(buf, r)
		}
	}

	buf = append(buf, '"')

	return string(buf)
}
