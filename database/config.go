package database

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgtype"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/dwagin/go-log"
)

//nolint:govet // config structure
type Config struct {
	LogLevel       *log.Level `env:"LOG_LEVEL" yaml:"log_level"`
	DSN            string     `env:"DSN" yaml:"dsn"`
	ConnectTimeout *int       `env:"CONNECT_TIMEOUT" yaml:"connect_timeout"`
}

func (v *Config) PgConnConfig() (*pgconn.Config, error) {
	connConfig, err := pgconn.ParseConfig(v.DSN)
	if err != nil {
		return nil, fmt.Errorf("dsn error: %v", err)
	}

	if v.ConnectTimeout != nil {
		connConfig.ConnectTimeout = time.Duration(*v.ConnectTimeout) * time.Millisecond
	}

	return connConfig, nil
}

func (v *Config) PoolConfig(logger *log.Logger) (*pgxpool.Config, error) {
	config, err := pgxpool.ParseConfig(v.DSN)
	if err != nil {
		return nil, fmt.Errorf("dsn error: %v", err)
	}

	config.AfterConnect = func(_ context.Context, conn *pgx.Conn) error {
		typeMap := conn.TypeMap()

		jsonbType := &pgtype.Type{
			Name: "jsonb", OID: pgtype.JSONBOID, Codec: &JSONBCodec{Unmarshal: json.Unmarshal},
		}
		jsonType := &pgtype.Type{
			Name: "json", OID: pgtype.JSONOID, Codec: &JSONCodec{Unmarshal: json.Unmarshal},
		}
		qCharType := &pgtype.Type{
			Name: "char", OID: pgtype.QCharOID, Codec: QCharCodec{},
		}

		jsonArrayType := &pgtype.Type{
			Name: "_json", OID: pgtype.JSONArrayOID, Codec: &pgtype.ArrayCodec{ElementType: jsonType},
		}
		jsonbArrayType := &pgtype.Type{
			Name: "_jsonb", OID: pgtype.JSONBArrayOID, Codec: &pgtype.ArrayCodec{ElementType: jsonbType},
		}
		qCharArrayType := &pgtype.Type{
			Name: "_char", OID: pgtype.QCharArrayOID, Codec: &pgtype.ArrayCodec{ElementType: qCharType},
		}

		typeMap.RegisterType(jsonbType)
		typeMap.RegisterType(jsonType)
		typeMap.RegisterType(qCharType)

		typeMap.RegisterType(jsonArrayType)
		typeMap.RegisterType(jsonbArrayType)
		typeMap.RegisterType(qCharArrayType)

		return nil
	}

	if v.ConnectTimeout != nil {
		config.ConnConfig.ConnectTimeout = time.Duration(*v.ConnectTimeout) * time.Millisecond
	}

	if logger != nil {
		if v.LogLevel != nil {
			logger = logger.New().WithLevel(*v.LogLevel).Logger()
		}

		config.ConnConfig.Tracer = newLogger(logger).TraceLog()
	}

	return config, nil
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.DSN == "" {
		return errors.New("'dsn' invalid value")
	}

	if v.ConnectTimeout != nil && *v.ConnectTimeout < 0 {
		return errors.New("'connect_timeout' invalid value")
	}

	return nil
}
