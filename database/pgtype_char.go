package database

import (
	"database/sql/driver"
	"fmt"

	"github.com/jackc/pgx/v5/pgtype"
)

type QCharCodec struct{}

func (QCharCodec) FormatSupported(format int16) bool {
	return format == pgtype.TextFormatCode || format == pgtype.BinaryFormatCode
}

func (QCharCodec) PreferredFormat() int16 {
	return pgtype.BinaryFormatCode
}

func (QCharCodec) PlanEncode(m *pgtype.Map, oid uint32, format int16, value any) pgtype.EncodePlan {
	switch format {
	case pgtype.TextFormatCode, pgtype.BinaryFormatCode:
		if _, ok := value.(string); ok {
			return encodePlanQCharCodecString{}
		}
	}

	return pgtype.QCharCodec{}.PlanEncode(m, oid, format, value)
}

type encodePlanQCharCodecString struct{}

func (encodePlanQCharCodecString) Encode(value any, buf []byte) ([]byte, error) {
	str, ok := value.(string)
	if !ok {
		return nil, fmt.Errorf("cannot encode %T", value)
	}

	if len(str) != 1 {
		return nil, fmt.Errorf(`%v cannot be encoded to "char"`, str)
	}

	buf = append(buf, str[0])

	return buf, nil
}

func (QCharCodec) PlanScan(m *pgtype.Map, oid uint32, format int16, target any) pgtype.ScanPlan {
	switch format {
	case pgtype.TextFormatCode, pgtype.BinaryFormatCode:
		if _, ok := target.(*string); ok {
			return scanPlanQCharCodecString{}
		}
	}

	return pgtype.QCharCodec{}.PlanScan(m, oid, format, target)
}

type scanPlanQCharCodecString struct{}

func (scanPlanQCharCodecString) Scan(src []byte, dst any) error {
	if src == nil {
		return fmt.Errorf("cannot scan NULL into %T", dst)
	}

	if len(src) > 1 {
		return fmt.Errorf(`invalid length for "char": %v`, len(src))
	}

	str, ok := dst.(*string)
	if !ok {
		return fmt.Errorf("cannot scan into %T", dst)
	}

	// In the text format the zero value is returned as a zero byte value instead of 0
	if len(src) == 0 {
		*str = string([]byte{0})
	} else {
		*str = string(src[0])
	}

	return nil
}

func (c QCharCodec) DecodeDatabaseSQLValue(m *pgtype.Map, oid uint32, format int16, src []byte) (driver.Value, error) {
	if src == nil {
		return nil, nil //nolint:nilnil // valid value
	}

	var s string
	if err := codecScan(c, m, oid, format, src, &s); err != nil {
		return nil, err
	}

	return s, nil
}

func (c QCharCodec) DecodeValue(m *pgtype.Map, oid uint32, format int16, src []byte) (any, error) {
	if src == nil {
		return nil, nil //nolint:nilnil // valid value
	}

	var s string
	if err := codecScan(c, m, oid, format, src, &s); err != nil {
		return nil, err
	}

	return s, nil
}
