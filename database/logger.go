package database

import (
	"context"
	"slices"

	pgx "github.com/jackc/pgx/v5/tracelog"
	"gitlab.com/dwagin/go-log"
)

type Logger struct {
	impl *log.Logger
}

func newLogger(logger *log.Logger) *Logger {
	return &Logger{
		impl: logger,
	}
}

func (v *Logger) TraceLog() *pgx.TraceLog {
	return &pgx.TraceLog{
		Logger:   v,
		LogLevel: v.Level(),
	}
}

func (v *Logger) Log(_ context.Context, level pgx.LogLevel, msg string, data map[string]any) {
	var logLevel log.Level

	switch level {
	case pgx.LogLevelNone:
		return
	case pgx.LogLevelError:
		logLevel = log.ErrorLevel
	case pgx.LogLevelWarn:
		logLevel = log.WarnLevel
	case pgx.LogLevelInfo:
		logLevel = log.InfoLevel
	case pgx.LogLevelDebug:
		logLevel = log.DebugLevel
	case pgx.LogLevelTrace:
		logLevel = log.TraceLevel
	default:
		logLevel = log.DebugLevel
	}

	if v.impl.Level() >= logLevel {
		logger := v.impl

		if len(data) > 0 {
			logContext := logger.New()

			keys := make([]string, 0, 32)
			for key := range data {
				keys = append(keys, key)
			}

			slices.Sort(keys)

			for i := range keys {
				logContext = logContext.WithField(keys[i], data[keys[i]])
			}

			logger = logContext.Logger()
			defer logger.Release()
		}

		logger.Log(logLevel, msg)
	}
}

func (v *Logger) Level() pgx.LogLevel {
	switch v.impl.Level() {
	case log.PanicLevel:
		return pgx.LogLevelError
	case log.FatalLevel:
		return pgx.LogLevelError
	case log.ErrorLevel:
		return pgx.LogLevelError
	case log.WarnLevel:
		return pgx.LogLevelWarn
	case log.NoticeLevel:
		return pgx.LogLevelInfo
	case log.InfoLevel:
		return pgx.LogLevelInfo
	case log.DebugLevel:
		return pgx.LogLevelDebug
	case log.TraceLevel:
		return pgx.LogLevelTrace
	default:
		return pgx.LogLevelDebug
	}
}
