package database

import (
	"errors"
	"fmt"
)

type JSONError struct {
	err error
}

func (e *JSONError) Error() string {
	return e.err.Error()
}

func (e *JSONError) Unwrap() error {
	return e.err
}

func newJSONError(text string) error {
	return &JSONError{err: errors.New(text)}
}

func newJSONErrorf(format string, a ...any) error {
	return &JSONError{err: fmt.Errorf(format, a...)}
}

var ErrUnexpectedNULL = newJSONError("unexpected null")
