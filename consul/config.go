package consul

import (
	"errors"

	"gitlab.com/dwagin/go-log"

	utilsHTTP "gitlab.com/dwagin/go-utils/http"
)

type ServiceConfig struct {
	LogLevel   *log.Level              `env:"LOG_LEVEL" yaml:"log_level"`
	HTTPConfig *utilsHTTP.ClientConfig `env:"HTTP_CONFIG" yaml:"http_config"`
	ID         string                  `env:"ID" yaml:"id"`
	Name       string                  `env:"NAME" yaml:"name"`
	Token      string                  `env:"TOKEN" yaml:"token"`
}

func (v *ServiceConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.ID == "" {
		return errors.New("'id' not defined")
	}

	if v.Name == "" {
		return errors.New("'name' not defined")
	}

	if v.Token == "" {
		return errors.New("'token' not defined")
	}

	return nil
}
