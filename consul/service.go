package consul

import (
	"context"
	"fmt"

	consulAPI "github.com/hashicorp/consul/api"
)

type Service struct {
	client *consulAPI.Client
	id     string
	name   string
}

func (v *ServiceConfig) New() (*Service, error) {
	var err error

	consul := &Service{
		id:   v.ID,
		name: v.Name,
	}

	clientConfig := consulAPI.DefaultConfig()
	clientConfig.Token = v.Token

	if v.HTTPConfig != nil {
		if clientConfig.HttpClient, err = v.HTTPConfig.Client(); err != nil {
			return nil, fmt.Errorf("http config error: %v", err)
		}
	}

	if consul.client, err = consulAPI.NewClient(clientConfig); err != nil {
		return nil, fmt.Errorf("new client error: %v", err)
	}

	return consul, nil
}

func (v *Service) Register(ctx context.Context, address string, port int) error {
	reg := &consulAPI.AgentServiceRegistration{
		ID:      v.id,
		Name:    v.name,
		Port:    port,
		Address: address,
	}

	opts := consulAPI.ServiceRegisterOpts{
		ReplaceExistingChecks: false,
	}

	if ctx != nil {
		opts = opts.WithContext(ctx)
	}

	if err := v.client.Agent().ServiceRegisterOpts(reg, opts); err != nil {
		return fmt.Errorf("register '%s:%d' error: %w", address, port, err)
	}

	return nil
}

func (v *Service) Deregister() error {
	if err := v.client.Agent().ServiceDeregister(v.id); err != nil {
		return fmt.Errorf("deregister error: %w", err)
	}

	return nil
}
