package logger

import (
	"fmt"
	"strings"

	"gitlab.com/dwagin/go-log"
)

func (v *Config) New() (*log.Logger, error) {
	var logContext *log.Context

	switch strings.ToLower(v.Dest) {
	case "empty":
		logContext = log.New(log.Empty())
	case "stdout":
		logContext = log.New(log.Stdout())
	case "windows":
		writer, err := log.Windows(v.Tag)
		if err != nil {
			return nil, fmt.Errorf("log writer error: %v", err)
		}

		logContext = log.New(writer)
	default:
		logContext = log.New(log.Console())
	}

	logContext = logContext.WithLevel(log.DebugLevel)

	if v.Level != nil {
		logContext = logContext.WithLevel(*v.Level)
	}

	return logContext.Logger(), nil
}
