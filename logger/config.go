package logger

import (
	"errors"
	"os"
	"path"
	"strings"

	"gitlab.com/dwagin/go-log"
)

type Config struct {
	Level *log.Level `env:"LEVEL" yaml:"level"`
	Dest  string     `env:"DESTINATION" yaml:"destination"`
	Tag   string     `env:"TAG" yaml:"tag"`
}

func (v *Config) Default() {
	*v = Config{
		Tag: path.Base(os.Args[0]),
	}
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Dest == "" {
		return errors.New("'destination' not defined")
	}

	if strings.ToLower(v.Dest) == "syslog" && v.Tag == "" {
		return errors.New("'tag' not defined")
	}

	if strings.ToLower(v.Dest) == "windows" && v.Tag == "" {
		return errors.New("'tag' not defined")
	}

	return nil
}
