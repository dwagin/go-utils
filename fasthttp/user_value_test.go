package fasthttp_test

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"github.com/valyala/fasthttp"

	utilsFastHTTP "gitlab.com/dwagin/go-utils/fasthttp"
)

type UserValueSuite struct {
	suite.Suite
}

var testUserValueCtx = func() *fasthttp.RequestCtx {
	userValues := map[string]any{
		"key0":  byte('0'),
		"key1":  []byte("value1"),
		"key2":  "value2",
		"key3":  int8(-127),
		"key4":  int16(32767),
		"key5":  int32(2147483647),
		"key6":  int64(-9223372036854775808),
		"key7":  true,
		"key8":  float32(942.392),
		"key9":  float64(-31283.45345392),
		"key10": nil,
	}

	ctx := &fasthttp.RequestCtx{}
	for key, value := range userValues {
		ctx.SetUserValue(key, value)
	}

	return ctx
}()

func TestUserValue(t *testing.T) {
	suite.Run(t, new(UserValueSuite))
}

func (v *UserValueSuite) TestByte() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[byte](testUserValueCtx, "key0")
	require.NoError(err)
	require.Equal(byte('0'), value)
}

func (v *UserValueSuite) TestByteSlice() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[[]byte](testUserValueCtx, "key1")
	require.NoError(err)
	require.Equal([]byte("value1"), value)
}

func (v *UserValueSuite) TestString() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[string](testUserValueCtx, "key2")
	require.NoError(err)
	require.Equal("value2", value)
}

func (v *UserValueSuite) TestInt8() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[int8](testUserValueCtx, "key3")
	require.NoError(err)
	require.Equal(int8(-127), value)
}

func (v *UserValueSuite) TestInt16() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[int16](testUserValueCtx, "key4")
	require.NoError(err)
	require.Equal(int16(32767), value)
}

func (v *UserValueSuite) TestInt32() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[int32](testUserValueCtx, "key5")
	require.NoError(err)
	require.Equal(int32(2147483647), value)
}

func (v *UserValueSuite) TestInt64() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[int64](testUserValueCtx, "key6")
	require.NoError(err)
	require.Equal(int64(-9223372036854775808), value)
}

func (v *UserValueSuite) TestBool() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[bool](testUserValueCtx, "key7")
	require.NoError(err)
	require.True(value)
}

func (v *UserValueSuite) TestFloat32() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[float32](testUserValueCtx, "key8")
	require.NoError(err)
	require.Equal(float32(942.392), value) //nolint:testifylint // must be same value
}

func (v *UserValueSuite) TestFloat64() {
	require := v.Require()
	value, err := utilsFastHTTP.UserValue[float64](testUserValueCtx, "key9")
	require.NoError(err)
	require.Equal(float64(-31283.45345392), value) //nolint:testifylint // must be same value
}

func (v *UserValueSuite) TestNonExistingKey() {
	var utilsErr *utilsFastHTTP.Error

	require := v.Require()
	_, err := utilsFastHTTP.UserValue[int](testUserValueCtx, "non-existing-key")
	require.Error(err)
	require.ErrorAs(err, &utilsErr)
	require.ErrorContains(err, "not contain")
}

func (v *UserValueSuite) TestNilValue() {
	var utilsErr *utilsFastHTTP.Error

	require := v.Require()
	_, err := utilsFastHTTP.UserValue[int](testUserValueCtx, "key10")
	require.Error(err)
	require.ErrorAs(err, &utilsErr)
	require.ErrorContains(err, "not contain")
}

func (v *UserValueSuite) TestWrongType() {
	var utilsErr *utilsFastHTTP.Error

	require := v.Require()
	_, err := utilsFastHTTP.UserValue[bool](testUserValueCtx, "key0")
	require.Error(err)
	require.ErrorAs(err, &utilsErr)
	require.ErrorContains(err, "wrong type")
}

func BenchmarkUserValue(b *testing.B) {
	b.Run("Serial", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		for range b.N {
			_, _ = utilsFastHTTP.UserValue[string](testUserValueCtx, "key2") //nolint:errcheck // tests
		}
	})
	b.Run("Concurrent", func(b *testing.B) {
		b.ReportAllocs()
		b.ResetTimer()

		b.RunParallel(func(pb *testing.PB) {
			for pb.Next() {
				_, _ = utilsFastHTTP.UserValue[string](testUserValueCtx, "key2") //nolint:errcheck // tests
			}
		})
	})
}
