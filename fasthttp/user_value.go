package fasthttp

import (
	"github.com/valyala/fasthttp"
)

//nolint:ireturn // special case
func UserValue[T any](ctx *fasthttp.RequestCtx, key string) (T, error) {
	var (
		ok  bool
		val any
		ret T
	)

	val = ctx.UserValue(key)
	if val == nil {
		return ret, NewErrorf(
			fasthttp.StatusInternalServerError,
			"ctx does not contain user value %q", key,
		)
	}

	if ret, ok = val.(T); !ok {
		return ret, NewErrorf(
			fasthttp.StatusInternalServerError,
			"ctx contains user value %q of the wrong type %T", key, val,
		)
	}

	return ret, nil
}
