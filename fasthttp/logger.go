package fasthttp

import (
	"gitlab.com/dwagin/go-log"
)

type Logger struct {
	impl *log.Logger
}

func newLogger(logger *log.Logger) *Logger {
	return &Logger{
		impl: logger,
	}
}

func (v *Logger) Printf(format string, args ...any) {
	v.impl.Warnf(format, args...)
}
