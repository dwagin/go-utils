package fasthttp

import (
	"errors"
	"fmt"
)

type Error struct {
	err  error
	code int
}

func (e *Error) StatusCode() int {
	return e.code
}

func (e *Error) Error() string {
	return e.err.Error()
}

func (e *Error) Unwrap() error {
	return e.err
}

func NewError(code int, text string) error {
	return &Error{code: code, err: errors.New(text)}
}

func NewErrorf(code int, format string, a ...any) error {
	return &Error{code: code, err: fmt.Errorf(format, a...)}
}
