package fasthttp

import (
	"crypto/tls"
	"errors"
	"fmt"
	"time"

	"github.com/valyala/fasthttp"
	"gitlab.com/dwagin/go-log"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

//nolint:govet // config structure
type ServerConfig struct {
	Network string `env:"NETWORK" yaml:"network"`
	Address string `env:"ADDRESS" yaml:"address"`
	Port    uint16 `env:"PORT" yaml:"port"`

	TLSConfigValue *utilsTLS.ServerConfig `env:"TLS_CONFIG" yaml:"tls_config"`

	ReadTimeout  *int `env:"READ_TIMEOUT" yaml:"read_timeout"`
	WriteTimeout *int `env:"WRITE_TIMEOUT" yaml:"write_timeout"`
	IdleTimeout  *int `env:"IDLE_TIMEOUT" yaml:"idle_timeout"`

	MaxRequestBodySize *int `env:"MAX_REQUEST_BODY_SIZE" yaml:"max_request_body_size"`

	DisablePreParseMultipartForm *bool `env:"DISABLE_PRE_PARSE_MULTIPART_FORM" yaml:"disable_pre_parse_multipart_form"`
	DisableDefaultServerHeader   *bool `env:"DISABLE_DEFAULT_SERVER_HEADER" yaml:"disable_default_server_header"`
	DisableDefaultDate           *bool `env:"DISABLE_DEFAULT_DATE" yaml:"disable_default_date"`
	DisableDefaultContentType    *bool `env:"DISABLE_DEFAULT_CONTENT_TYPE" yaml:"disable_default_content_type"`
}

func (v *ServerConfig) Server(logger *log.Logger) *fasthttp.Server {
	server := &fasthttp.Server{
		ReadTimeout:                  1 * time.Minute,
		WriteTimeout:                 1 * time.Minute,
		IdleTimeout:                  10 * time.Minute,
		MaxRequestBodySize:           10 * 1024 * 1024,
		DisablePreParseMultipartForm: true,
		SecureErrorLogMessage:        true,
		NoDefaultServerHeader:        true,
		NoDefaultDate:                true,
		NoDefaultContentType:         true,
		CloseOnShutdown:              true,
		StreamRequestBody:            false,
	}

	if logger != nil {
		server.Logger = newLogger(logger)
	}

	if v.ReadTimeout != nil {
		server.ReadTimeout = time.Duration(*v.ReadTimeout) * time.Millisecond
	}

	if v.WriteTimeout != nil {
		server.WriteTimeout = time.Duration(*v.WriteTimeout) * time.Millisecond
	}

	if v.IdleTimeout != nil {
		server.IdleTimeout = time.Duration(*v.IdleTimeout) * time.Millisecond
	}

	if v.MaxRequestBodySize != nil {
		server.MaxRequestBodySize = *v.MaxRequestBodySize
	}

	if v.DisablePreParseMultipartForm != nil {
		server.DisablePreParseMultipartForm = *v.DisablePreParseMultipartForm
	}

	if v.DisableDefaultServerHeader != nil {
		server.NoDefaultServerHeader = *v.DisableDefaultServerHeader
	}

	if v.DisableDefaultDate != nil {
		server.NoDefaultDate = *v.DisableDefaultDate
	}

	if v.DisableDefaultContentType != nil {
		server.NoDefaultContentType = *v.DisableDefaultContentType
	}

	return server
}

func (v *ServerConfig) TLSConfig() (*tls.Config, error) {
	if v.TLSConfigValue != nil {
		tlsConfig, err := v.TLSConfigValue.TLSConfig()
		if err != nil {
			return nil, fmt.Errorf("tls config error: %v", err)
		}

		return tlsConfig, nil
	}

	return nil, nil //nolint:nilnil // valid value
}

func (v *ServerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.ReadTimeout != nil && *v.ReadTimeout < 0 {
		return errors.New("'read_timeout' invalid value")
	}

	if v.WriteTimeout != nil && *v.WriteTimeout < 0 {
		return errors.New("'write_timeout' invalid value")
	}

	if v.IdleTimeout != nil && *v.IdleTimeout < 1000 {
		return errors.New("'idle_timeout' invalid value")
	}

	if v.MaxRequestBodySize != nil && *v.MaxRequestBodySize < 0 {
		return errors.New("'max_request_body_size' invalid value")
	}

	return nil
}
