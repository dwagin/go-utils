package fasthttp

import (
	"errors"
	"fmt"
	"net"
	"time"

	"github.com/valyala/fasthttp"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

type ClientConfig struct {
	TLSConfig *utilsTLS.ClientConfig `env:"TLS_CONFIG" yaml:"tls_config"`

	MaxConnsPerHost           *int `env:"MAX_CONNS_PER_HOST" yaml:"max_conns_per_host"`
	MaxIdleConnDuration       *int `env:"MAX_IDLE_CONN_DURATION" yaml:"max_idle_conn_duration"`
	MaxConnDuration           *int `env:"MAX_CONN_DURATION" yaml:"max_conn_duration"`
	MaxIdempotentCallAttempts *int `env:"MAX_IDEMPOTENT_CALL_ATTEMPTS" yaml:"max_idempotent_call_attempts"`

	DialTimeout  *int `env:"DIAL_TIMEOUT" yaml:"dial_timeout"`
	ReadTimeout  *int `env:"READ_TIMEOUT" yaml:"read_timeout"`
	WriteTimeout *int `env:"WRITE_TIMEOUT" yaml:"write_timeout"`

	MaxResponseBodySize *int `env:"MAX_RESPONSE_BODY_SIZE" yaml:"max_response_body_size"`
	MaxConnWaitTimeout  *int `env:"MAX_CONN_WAIT_TIMEOUT" yaml:"max_conn_wait_timeout"`
}

func (v *ClientConfig) Client() (*fasthttp.Client, error) {
	var err error

	client := &fasthttp.Client{
		MaxConnsPerHost:           fasthttp.DefaultMaxConnsPerHost,
		MaxIdleConnDuration:       fasthttp.DefaultMaxIdleConnDuration,
		MaxIdemponentCallAttempts: fasthttp.DefaultMaxIdemponentCallAttempts,
		ReadTimeout:               5 * time.Second,
		WriteTimeout:              5 * time.Second,
		MaxConnWaitTimeout:        1 * time.Second,
		NoDefaultUserAgentHeader:  true,
	}

	if v.TLSConfig != nil {
		if client.TLSConfig, err = v.TLSConfig.TLSConfig(); err != nil {
			return nil, fmt.Errorf("tls config error: %v", err)
		}
	}

	if v.MaxConnsPerHost != nil {
		client.MaxConnsPerHost = *v.MaxConnsPerHost
	}

	if v.MaxIdleConnDuration != nil {
		client.MaxIdleConnDuration = time.Duration(*v.MaxIdleConnDuration) * time.Millisecond
	}

	if v.MaxConnDuration != nil {
		client.MaxConnDuration = time.Duration(*v.MaxConnDuration) * time.Millisecond
	}

	if v.MaxIdempotentCallAttempts != nil {
		client.MaxIdemponentCallAttempts = *v.MaxIdempotentCallAttempts
	}

	if v.DialTimeout != nil {
		timeout := time.Duration(*v.DialTimeout) * time.Millisecond
		if timeout != fasthttp.DefaultDialTimeout {
			client.Dial = func(addr string) (net.Conn, error) {
				return fasthttp.DialTimeout(addr, timeout)
			}
		}
	}

	if v.ReadTimeout != nil {
		client.ReadTimeout = time.Duration(*v.ReadTimeout) * time.Millisecond
	}

	if v.WriteTimeout != nil {
		client.WriteTimeout = time.Duration(*v.WriteTimeout) * time.Millisecond
	}

	if v.MaxResponseBodySize != nil {
		client.MaxResponseBodySize = *v.MaxResponseBodySize
	}

	if v.MaxConnWaitTimeout != nil {
		client.MaxConnWaitTimeout = time.Duration(*v.MaxConnWaitTimeout) * time.Millisecond
	}

	return client, nil
}

func (v *ClientConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.MaxConnsPerHost != nil && *v.MaxConnsPerHost < 0 {
		return errors.New("'max_conns_per_host' invalid value")
	}

	if v.MaxIdleConnDuration != nil && *v.MaxIdleConnDuration < 0 {
		return errors.New("'max_idle_conn_duration' invalid value")
	}

	if v.MaxConnDuration != nil && *v.MaxConnDuration < 0 {
		return errors.New("'max_conn_duration' invalid value")
	}

	if v.MaxIdempotentCallAttempts != nil && *v.MaxIdempotentCallAttempts < 0 {
		return errors.New("'max_idempotent_call_attempts' invalid value")
	}

	if v.DialTimeout != nil && *v.DialTimeout < 0 {
		return errors.New("'dial_timeout' invalid value")
	}

	if v.ReadTimeout != nil && *v.ReadTimeout < 0 {
		return errors.New("'read_timeout' invalid value")
	}

	if v.WriteTimeout != nil && *v.WriteTimeout < 0 {
		return errors.New("'write_timeout' invalid value")
	}

	if v.MaxResponseBodySize != nil && *v.MaxResponseBodySize < 0 {
		return errors.New("'max_response_body_size' invalid value")
	}

	if v.MaxConnWaitTimeout != nil && *v.MaxConnWaitTimeout < 0 {
		return errors.New("'max_conn_wait_timeout' invalid value")
	}

	return nil
}
