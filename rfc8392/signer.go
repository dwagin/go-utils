package rfc8392

import (
	"crypto"
	"crypto/rand"
	"fmt"

	"github.com/veraison/go-cose"
)

type Signer struct {
	signer cose.Signer
}

func NewSigner(alg Algorithm, key crypto.Signer) (*Signer, error) {
	var (
		err    error
		signer cose.Signer
	)

	switch alg {
	case AlgorithmEdDSA:
		signer, err = cose.NewSigner(cose.AlgorithmEdDSA, key)
	default:
		err = cose.ErrAlgorithmNotSupported
	}

	if err != nil {
		return nil, fmt.Errorf("cose error: %v", err)
	}

	return &Signer{signer: signer}, nil
}

func (s *Signer) Sign(data any) ([]byte, error) {
	var (
		err     error
		payload []byte
		raw     []byte
	)

	payload, err = encMode.Marshal(data)
	if err != nil {
		return nil, fmt.Errorf("payload marshaling error: %v", err)
	}

	msg := cose.Sign1Message{
		Headers: cose.Headers{
			Protected: cose.ProtectedHeader{
				cose.HeaderLabelAlgorithm: s.signer.Algorithm(),
			},
		},
		Payload: payload,
	}

	err = msg.Sign(rand.Reader, nil, s.signer)
	if err != nil {
		return nil, fmt.Errorf("message sign error: %v", err)
	}

	raw, err = msg.MarshalCBOR()
	if err != nil {
		return nil, fmt.Errorf("message marshaling error: %v", err)
	}

	return raw, nil
}
