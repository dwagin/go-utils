package rfc8392

import (
	"crypto"
	"fmt"

	"github.com/veraison/go-cose"
)

type Verifier struct {
	verifier cose.Verifier
}

func NewVerifier(alg Algorithm, key crypto.PublicKey) (*Verifier, error) {
	var (
		err      error
		verifier cose.Verifier
	)

	switch alg {
	case AlgorithmEdDSA:
		verifier, err = cose.NewVerifier(cose.AlgorithmEdDSA, key)
	default:
		err = cose.ErrAlgorithmNotSupported
	}

	if err != nil {
		return nil, fmt.Errorf("cose error: %v", err)
	}

	return &Verifier{verifier: verifier}, nil
}

func (v *Verifier) Verify(raw []byte, data any) error {
	var (
		err error
		msg cose.Sign1Message
	)

	err = msg.UnmarshalCBOR(raw)
	if err != nil {
		return fmt.Errorf("message unmarshaling error: %v", err)
	}

	err = msg.Verify(nil, v.verifier)
	if err != nil {
		return fmt.Errorf("message verify error: %v", err)
	}

	err = decMode.Unmarshal(msg.Payload, data)
	if err != nil {
		return fmt.Errorf("payload unmarshaling error: %v", err)
	}

	return nil
}
