package rfc8392

type (
	Iss string
	Sub string
	Aud string
	Exp int64
	Nbf int64
	Iat int64
	Cti []byte
)
