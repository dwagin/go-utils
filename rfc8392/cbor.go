package rfc8392

import (
	"github.com/fxamacker/cbor/v2"
)

var (
	encMode cbor.EncMode
	decMode cbor.DecMode
)

//nolint:gochecknoinits // init cbor
func init() {
	var err error

	encOpts := cbor.EncOptions{
		Sort:        cbor.SortCanonical,        // sort map keys
		IndefLength: cbor.IndefLengthForbidden, // no streaming
	}

	encMode, err = encOpts.EncMode()
	if err != nil {
		panic(err)
	}

	decOpts := cbor.DecOptions{
		DupMapKey:        cbor.DupMapKeyEnforcedAPF,      // duplicated key not allowed
		MaxNestedLevels:  8,                              // max nested levels allowed
		MaxArrayElements: 128,                            // max number of elements for CBOR arrays
		MaxMapPairs:      128,                            // max number of key-value pairs for CBOR maps
		IndefLength:      cbor.IndefLengthForbidden,      // no streaming
		IntDec:           cbor.IntDecConvertSignedOrFail, // decode CBOR uint/int to Go int64
	}

	decMode, err = decOpts.DecMode()
	if err != nil {
		panic(err)
	}
}
