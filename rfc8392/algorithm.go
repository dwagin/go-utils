package rfc8392

import (
	"fmt"

	"github.com/veraison/go-cose"
)

// Algorithms supported by this library.
const (
	// AlgorithmEdDSA PureEdDSA by RFC 8152.
	AlgorithmEdDSA = Algorithm(cose.AlgorithmEdDSA)
)

type Algorithm int64

// String returns the name of the algorithm.
func (v Algorithm) String() string {
	switch v {
	case AlgorithmEdDSA:
		return "EdDSA"
	default:
		return fmt.Sprintf("unknown algorithm value %d", v)
	}
}
