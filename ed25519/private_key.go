package ed25519

import (
	cryptoED25519 "crypto/ed25519"
	"encoding/base64"
	"fmt"
)

type PrivateKey cryptoED25519.PrivateKey

func (v *PrivateKey) UnmarshalText(text []byte) error {
	dst := make([]byte, base64.StdEncoding.DecodedLen(len(text)))

	n, err := base64.StdEncoding.Decode(dst, text)
	if err != nil {
		return fmt.Errorf("base64 decode error: %v", err)
	}

	if n != cryptoED25519.SeedSize {
		return fmt.Errorf("wrong PrivateKey Seed size: got %d, expected %d", n, cryptoED25519.SeedSize)
	}

	dst = dst[:n]

	*v = PrivateKey(cryptoED25519.NewKeyFromSeed(dst))

	return nil
}

func (v *PrivateKey) Public() PublicKey {
	//nolint:errcheck,forcetypeassert // redundant
	return PublicKey(cryptoED25519.PrivateKey(*v).Public().(cryptoED25519.PublicKey))
}
