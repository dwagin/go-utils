package ed25519

import (
	cryptoED25519 "crypto/ed25519"
	"encoding/base64"
	"fmt"
)

type PublicKey cryptoED25519.PublicKey

func (v *PublicKey) UnmarshalText(text []byte) error {
	dst := make([]byte, base64.StdEncoding.DecodedLen(len(text)))

	n, err := base64.StdEncoding.Decode(dst, text)
	if err != nil {
		return fmt.Errorf("base64 decode error: %v", err)
	}

	if n != cryptoED25519.PublicKeySize {
		return fmt.Errorf("wrong PublicKey size: got %d, expected %d", n, cryptoED25519.PublicKeySize)
	}

	dst = dst[:n]

	*v = dst

	return nil
}
