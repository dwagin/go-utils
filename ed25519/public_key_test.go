package ed25519_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	utilsED25519 "gitlab.com/dwagin/go-utils/ed25519"
)

func TestPublicKey(t *testing.T) {
	t.Parallel()

	t.Run("UnmarshalText", func(t *testing.T) {
		t.Parallel()

		testCases := []struct { //nolint:govet // tests
			name        string
			value       []byte
			expected    utilsED25519.PublicKey
			expectedErr error
		}{
			{
				name:  "Valid",
				value: []byte("YNLgfTkKgu1/RedbdzwM9nCpwyheOpHrSuql45J9cKQ="),
				expected: utilsED25519.PublicKey{
					0x60, 0xd2, 0xe0, 0x7d, 0x39, 0xa, 0x82, 0xed, 0x7f, 0x45, 0xe7, 0x5b, 0x77, 0x3c, 0xc, 0xf6,
					0x70, 0xa9, 0xc3, 0x28, 0x5e, 0x3a, 0x91, 0xeb, 0x4a, 0xea, 0xa5, 0xe3, 0x92, 0x7d, 0x70, 0xa4,
				},
			},
			{
				name:        "Wrong size with empty value",
				value:       []byte(""),
				expectedErr: errors.New("wrong PublicKey size"),
			},
			{
				name:        "Wrong size with nil value",
				value:       nil,
				expectedErr: errors.New("wrong PublicKey size"),
			},
			{
				name:        "Illegal base64 data #1",
				value:       []byte("бэйс64"),
				expectedErr: errors.New("illegal base64 data at input byte 0"),
			},
			{
				name:        "Illegal base64 data #2",
				value:       []byte("Q-FsbGJhY2s="),
				expectedErr: errors.New("illegal base64 data at input byte 1"),
			},
			{
				name:        "Illegal base64 data #3",
				value:       []byte("Q2FsbGJhY2s"),
				expectedErr: errors.New("illegal base64 data at input byte 8"),
			},
		}

		for i := range testCases {
			test := testCases[i]

			t.Run(test.name, func(t *testing.T) {
				t.Parallel()

				require := require.New(t)

				var actual utilsED25519.PublicKey

				err := actual.UnmarshalText(test.value)
				if test.expectedErr == nil {
					require.NoError(err)
					require.Equal(test.expected, actual)
				} else {
					require.Error(err)
					require.ErrorContains(err, test.expectedErr.Error())
				}
			})
		}
	})
}
