package config

import (
	"fmt"
	"os"

	"gitlab.com/dwagin/go-envconfig"
	"gitlab.com/dwagin/go-log"

	utilsLogger "gitlab.com/dwagin/go-utils/logger"
)

type Complete interface {
	Default()
	Validate() error
	LoggerConfig() *utilsLogger.Config
}

func Load(config Complete) (*log.Logger, error) {
	config.Default()

	if len(os.Args) > 1 {
		fileName := os.Args[1]
		if err := envconfig.ReadConfig(config, fileName); err != nil {
			return nil, fmt.Errorf("config from file error: %v", err)
		}
	}

	if err := envconfig.ReadEnv(config, "APP"); err != nil {
		return nil, fmt.Errorf("config from env error: %v", err)
	}

	if err := config.Validate(); err != nil {
		return nil, fmt.Errorf("config validate error: %v", err)
	}

	logger, err := config.LoggerConfig().New()
	if err != nil {
		return nil, fmt.Errorf("create logger error: %v", err)
	}

	return logger, nil
}
