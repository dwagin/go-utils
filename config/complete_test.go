package config_test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	utilsConfig "gitlab.com/dwagin/go-utils/config"
	utilsGeneral "gitlab.com/dwagin/go-utils/general"
	utilsLogger "gitlab.com/dwagin/go-utils/logger"
)

type Config struct {
	General *utilsGeneral.Config `yaml:"general"`
}

func (v *Config) Default() {
	*v = Config{
		General: &utilsGeneral.Config{},
	}
	v.General.Default()
}

func (v *Config) LoggerConfig() *utilsLogger.Config {
	return v.General.Log
}

func (v *Config) Validate() error {
	return nil
}

func TestConfig_Load(t *testing.T) {
	t.Parallel()

	os.Args = []string{"config_test", "complete_test.yml"}

	c := &Config{}

	logger, err := utilsConfig.Load(c)
	require.NoError(t, err)
	logger.Close()
}
