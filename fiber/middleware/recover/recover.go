package recover //nolint:predeclared // catch panics

import (
	"bufio"
	"bytes"
	"runtime/debug"

	"github.com/gofiber/fiber/v2"
)

const maxStackLen = 20

type Handler = func(*fiber.Ctx, any, []string) error

func New(options ...Option) fiber.Handler {
	cfg := &config{
		stackTrace: true,
	}

	for i := range options {
		options[i](cfg)
	}

	handler := cfg.handler
	stackTrace := cfg.stackTrace

	return func(c *fiber.Ctx) (err error) {
		defer func() {
			if r := recover(); r != nil {
				if handler != nil {
					var stack []string

					if stackTrace {
						scanner := bufio.NewScanner(bytes.NewBuffer(debug.Stack()))
						for lineCount := 0; scanner.Scan(); lineCount++ {
							if lineCount > 0 && lineCount < 7 {
								continue
							}

							stack = append(stack, scanner.Text())

							if len(stack) == maxStackLen {
								break
							}
						}
					}

					err = handler(c, r, stack)
				} else {
					err = fiber.ErrInternalServerError
				}
			}
		}()

		return c.Next()
	}
}
