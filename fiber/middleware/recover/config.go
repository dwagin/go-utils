package recover //nolint:predeclared // catch panics

type config struct {
	handler    Handler
	stackTrace bool
}

type Option = func(*config)

func WithHandler(handler Handler) Option {
	return func(v *config) { v.handler = handler }
}

func WithoutStackTrace() Option {
	return func(v *config) { v.stackTrace = false }
}
