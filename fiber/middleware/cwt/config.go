package cwt

import (
	"encoding/base64"
	"strings"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/dwagin/go-utils/rfc8392"
)

//nolint:govet // config structure
type config struct {
	verifier *rfc8392.Verifier

	tokenCustomSource TokenFunc
	tokenAuthScheme   string
	tokenSourceKey    string
	tokenSourceType   TokenSourceType

	contextKey string
}

func (cfg *config) defaults() {
	if cfg.tokenCustomSource == nil {
		if cfg.tokenSourceKey == "" {
			if cfg.tokenSourceType == Header {
				cfg.tokenSourceKey = fiber.HeaderAuthorization
				if cfg.tokenAuthScheme == "" {
					cfg.tokenAuthScheme = "Bearer"
				}
			} else {
				cfg.tokenSourceKey = "token"
			}
		}
	}

	if cfg.contextKey == "" {
		cfg.contextKey = "token"
	}
}

func (cfg *config) tokenFunc() TokenFunc {
	if cfg.tokenCustomSource != nil {
		return cfg.tokenCustomSource
	}

	tokenSourceKey := cfg.tokenSourceKey

	switch cfg.tokenSourceType {
	case Header:
		prefix := strings.TrimSpace(cfg.tokenAuthScheme) + " "

		return func(c *fiber.Ctx) ([]byte, error) {
			val := c.Get(tokenSourceKey)
			if len(val) <= len(prefix) || val[0:len(prefix)] != prefix {
				return nil, ErrMissingToken
			}

			return base64.RawURLEncoding.DecodeString(val[len(prefix):])
		}
	case Query:
		return func(c *fiber.Ctx) ([]byte, error) {
			token := c.Query(tokenSourceKey)
			if token == "" {
				return nil, ErrMissingToken
			}

			return base64.RawURLEncoding.DecodeString(token)
		}
	case Param:
		return func(c *fiber.Ctx) ([]byte, error) {
			token := c.Params(tokenSourceKey)
			if token == "" {
				return nil, ErrMissingToken
			}

			return base64.RawURLEncoding.DecodeString(token)
		}
	case Cookie:
		return func(c *fiber.Ctx) ([]byte, error) {
			token := c.Cookies(tokenSourceKey)
			if token == "" {
				return nil, ErrMissingToken
			}

			return base64.RawURLEncoding.DecodeString(token)
		}
	default:
		panic("unknown token source")
	}
}

type Option = func(*config)

func WithVerifier(verifier *rfc8392.Verifier) Option {
	return func(v *config) { v.verifier = verifier }
}

func WithCustomTokenSource(tokenFunc TokenFunc) Option {
	return func(v *config) { v.tokenCustomSource = tokenFunc }
}

func WithTokenAuthScheme(tokenAuthScheme string) Option {
	return func(v *config) { v.tokenAuthScheme = tokenAuthScheme }
}

func WithTokenSource(tokenSourceType TokenSourceType) Option {
	return func(v *config) { v.tokenSourceType = tokenSourceType }
}

func WithTokenSourceKey(tokenSourceName string) Option {
	return func(v *config) { v.tokenSourceKey = tokenSourceName }
}

func WithContextKey(contextKey string) Option {
	return func(v *config) { v.contextKey = contextKey }
}
