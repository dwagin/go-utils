package cwt

type Claims[T any] interface {
	Default()
	Validate() error
	*T
}
