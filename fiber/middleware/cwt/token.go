package cwt

import "github.com/gofiber/fiber/v2"

type TokenSourceType int

const (
	Header TokenSourceType = iota
	Query
	Param
	Cookie
)

type TokenFunc func(c *fiber.Ctx) ([]byte, error)
