package cwt

import (
	"github.com/gofiber/fiber/v2"
)

func New[T any, PT Claims[T]](options ...Option) fiber.Handler {
	var err error

	cfg := &config{}

	for i := range options {
		options[i](cfg)
	}

	cfg.defaults()

	if cfg.verifier == nil {
		panic("CWT verifier not defined")
	}

	tokenFunc := cfg.tokenFunc()
	verifier := cfg.verifier
	contextKey := cfg.contextKey

	return func(c *fiber.Ctx) error {
		var token []byte

		if token, err = tokenFunc(c); err != nil {
			return newErrorf("get token error: %v", err)
		}

		claims := PT(new(T))
		claims.Default()

		if err = verifier.Verify(token, claims); err != nil {
			return newErrorf("verify token error: %v", err)
		}

		if err = claims.Validate(); err != nil {
			return newErrorf("validate token error: %v", err)
		}

		c.Locals(contextKey, claims)

		return c.Next()
	}
}
