package cwt

import (
	"errors"
	"fmt"
)

type Error struct {
	err error
}

func (e *Error) Error() string {
	return e.err.Error()
}

func (e *Error) Unwrap() error {
	return e.err
}

func newError(text string) error {
	return &Error{err: errors.New(text)}
}

func newErrorf(format string, a ...any) error {
	return &Error{err: fmt.Errorf(format, a...)}
}

var ErrMissingToken = newError("missing or malformed token")
