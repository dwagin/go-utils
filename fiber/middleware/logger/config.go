package logger

type config struct {
	handler Handler
}

type Option = func(*config)

func WithHandler(handler Handler) Option {
	return func(v *config) { v.handler = handler }
}
