package logger

import (
	"sync"

	"github.com/gofiber/fiber/v2"
)

type Handler = func(*fiber.Ctx)

func New(options ...Option) fiber.Handler {
	var (
		once       sync.Once
		errHandler fiber.ErrorHandler
	)

	cfg := &config{}

	for i := range options {
		options[i](cfg)
	}

	if cfg.handler != nil {
		handler := cfg.handler

		return func(c *fiber.Ctx) error {
			once.Do(func() {
				errHandler = c.App().ErrorHandler
			})

			chainErr := c.Next()

			if chainErr != nil {
				if err := errHandler(c, chainErr); err != nil {
					c.SendStatus(fiber.StatusInternalServerError) //nolint:errcheck // error ignored
				}
			}

			handler(c)

			return nil
		}
	}

	return func(*fiber.Ctx) error { return nil }
}
