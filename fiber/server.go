package fiber

import (
	"crypto/tls"
	"errors"
	"fmt"
	"time"

	"github.com/gofiber/fiber/v2"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

//nolint:govet // config structure
type ServerConfig struct {
	Network string `env:"NETWORK" yaml:"network"`
	Address string `env:"ADDRESS" yaml:"address"`
	Port    uint16 `env:"PORT" yaml:"port"`

	TLSConfigValue *utilsTLS.ServerConfig `env:"TLS_CONFIG" yaml:"tls_config"`

	ProxyForwardedHeader string `env:"PROXY_FORWARDED_HEADER" yaml:"proxy_forwarded_header"`

	ReadTimeout  *int `env:"READ_TIMEOUT" yaml:"read_timeout"`
	WriteTimeout *int `env:"WRITE_TIMEOUT" yaml:"write_timeout"`
	IdleTimeout  *int `env:"IDLE_TIMEOUT" yaml:"idle_timeout"`

	DisableDefaultDate        bool `env:"DISABLE_DEFAULT_DATE" yaml:"disable_default_date"`
	DisableDefaultContentType bool `env:"DISABLE_DEFAULT_CONTENT_TYPE" yaml:"disable_default_content_type"`
}

func (v *ServerConfig) FiberServerConfig() fiber.Config {
	config := fiber.Config{
		Network:                   v.Network,
		ProxyHeader:               v.ProxyForwardedHeader,
		DisableDefaultDate:        v.DisableDefaultDate,
		DisableDefaultContentType: v.DisableDefaultContentType,
		DisableStartupMessage:     true,
	}

	if v.ReadTimeout != nil {
		config.ReadTimeout = time.Duration(*v.ReadTimeout) * time.Millisecond
	}

	if v.WriteTimeout != nil {
		config.WriteTimeout = time.Duration(*v.WriteTimeout) * time.Millisecond
	}

	if v.IdleTimeout != nil {
		config.IdleTimeout = time.Duration(*v.IdleTimeout) * time.Millisecond
	}

	return config
}

func (v *ServerConfig) TLSConfig() (*tls.Config, error) {
	if v.TLSConfigValue != nil {
		tlsConfig, err := v.TLSConfigValue.TLSConfig()
		if err != nil {
			return nil, fmt.Errorf("tls config error: %v", err)
		}

		return tlsConfig, nil
	}

	return nil, nil //nolint:nilnil // valid value
}

func (v *ServerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Address == "" {
		return errors.New("'address' not defined")
	}

	if v.Port < 1 {
		return errors.New("'port' invalid value")
	}

	if v.ReadTimeout != nil && *v.ReadTimeout < 0 {
		return errors.New("'read_timeout' invalid value")
	}

	if v.WriteTimeout != nil && *v.WriteTimeout < 0 {
		return errors.New("'write_timeout' invalid value")
	}

	if v.IdleTimeout != nil && *v.IdleTimeout < 0 {
		return errors.New("'idle_timeout' invalid value")
	}

	if v.IdleTimeout != nil && *v.IdleTimeout < 1000 {
		return errors.New("'idle_timeout' too small")
	}

	return nil
}
