package oracle_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-utils/oracle"
)

func TestDSN_UnmarshalText(t *testing.T) {
	t.Parallel()

	testCases := []struct { //nolint:govet // tests
		name        string
		value       []byte
		expected    string
		expectedErr error
	}{
		{
			name:     "DSN with # in password",
			value:    []byte("oracle://user:p#assword@test:1521/test"),
			expected: "oracle://user:p%23assword@test:1521/test",
		},
		{
			name:     "DSN with # in username",
			value:    []byte("oracle://us#er:password@test:1521/test"),
			expected: "oracle://us%23er:password@test:1521/test",
		},
		{
			name:     "DSN with many special characters",
			value:    []byte("oracle://us:#er:<p!@ssword>@test:1521/test"),
			expected: "oracle://us%3A%23er:%3Cp%21%40ssword%3E@test:1521/test",
		},
		{
			name:     "DSN with escaped and special characters",
			value:    []byte("oracle://us%23er#:p%40sswo@rd@test:1521/test"),
			expected: "oracle://us%23er%23:p%40sswo%40rd@test:1521/test",
		},
		{
			name:     "DSN with %",
			value:    []byte("oracle://#user%:password@test:1521/test"),
			expected: "oracle://%23user%25:password@test:1521/test",
		},
		{
			name:        "Invalid DSN",
			value:       []byte("aaa"),
			expectedErr: errors.New("invalid value"),
		},
	}

	for i := range testCases {
		test := testCases[i]

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			require := require.New(t)

			var actual oracle.DSN

			err := actual.UnmarshalText(test.value)
			if test.expectedErr == nil {
				require.NoError(err)
				require.Equal(test.expected, string(actual))
			} else {
				require.Error(err)
				require.ErrorContains(err, test.expectedErr.Error())
			}
		})
	}
}
