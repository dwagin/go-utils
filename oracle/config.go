package oracle

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/sijms/go-ora/v2" // sql driver
	sqlLog "github.com/simukti/sqldb-logger"
	"gitlab.com/dwagin/go-log"
)

//nolint:govet // config structure
type Config struct {
	LogLevel        *log.Level `env:"LOG_LEVEL" yaml:"log_level"`
	DSN             DSN        `env:"DSN" yaml:"dsn"`
	MaxOpenConns    *int       `env:"MAX_OPEN_CONNS" yaml:"max_open_conns"`
	MaxIdleConns    *int       `env:"MAX_IDLE_CONNS" yaml:"max_idle_conns"`
	ConnMaxLifetime *int       `env:"CONN_MAX_LIFETIME" yaml:"conn_max_lifetime"`
	ConnMaxIdleTime *int       `env:"CONN_MAX_IDLE_TIME" yaml:"conn_max_idle_time"`
}

func (v *Config) DB(logger *log.Logger) (*sql.DB, error) {
	db, err := sql.Open("oracle", string(v.DSN))
	if err != nil {
		return nil, fmt.Errorf("open database error: %v", err)
	}

	if logger != nil {
		if v.LogLevel != nil {
			logger = logger.New().WithLevel(*v.LogLevel).Logger()
		}

		db = sqlLog.OpenDriver(string(v.DSN), db.Driver(), newLogger(logger))
	}

	if v.MaxOpenConns != nil {
		db.SetMaxOpenConns(*v.MaxOpenConns)
	}

	if v.MaxIdleConns != nil {
		db.SetMaxIdleConns(*v.MaxIdleConns)
	}

	if v.ConnMaxLifetime != nil {
		db.SetConnMaxLifetime(time.Duration(*v.ConnMaxLifetime) * time.Millisecond)
	}

	if v.ConnMaxIdleTime != nil {
		db.SetConnMaxIdleTime(time.Duration(*v.ConnMaxIdleTime) * time.Millisecond)
	}

	return db, nil
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.DSN == "" {
		return errors.New("'dsn' invalid value")
	}

	if v.MaxOpenConns != nil && *v.MaxOpenConns < 0 {
		return errors.New("'max_open_conns' invalid value")
	}

	if v.MaxIdleConns != nil && *v.MaxIdleConns < 0 {
		return errors.New("'max_idle_conns' invalid value")
	}

	if v.MaxOpenConns != nil && v.MaxIdleConns != nil && *v.MaxIdleConns > *v.MaxOpenConns {
		return errors.New("'max_idle_conns' must be less than or equal to 'max_open_conns'")
	}

	if v.ConnMaxLifetime != nil && *v.ConnMaxLifetime < 0 {
		return errors.New("'conn_max_lifetime' invalid value")
	}

	if v.ConnMaxLifetime != nil && *v.ConnMaxLifetime < 1000 {
		return errors.New("'conn_max_lifetime' too small")
	}

	if v.ConnMaxIdleTime != nil && *v.ConnMaxIdleTime < 0 {
		return errors.New("'conn_max_idle_time' invalid value")
	}

	if v.ConnMaxIdleTime != nil && *v.ConnMaxIdleTime < 1000 {
		return errors.New("'conn_max_idle_time' too small")
	}

	return nil
}
