package oracle

import (
	"database/sql"

	"gitlab.com/dwagin/go-log"
)

type Database struct {
	db *sql.DB
}

func (v *Config) New(logger *log.Logger) (*Database, error) {
	logger = logger.New().WithName("database").Logger()

	db, err := v.DB(logger)
	if err != nil {
		return nil, err
	}

	return &Database{db: db}, nil
}

func (v *Database) DB() *sql.DB {
	return v.db
}

func (v *Database) Shutdown() {
	v.db.Close()
}
