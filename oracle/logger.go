package oracle

import (
	"context"
	"slices"

	sqlLog "github.com/simukti/sqldb-logger"
	"gitlab.com/dwagin/go-log"
)

type Logger struct {
	impl *log.Logger
}

func newLogger(logger *log.Logger) *Logger {
	return &Logger{
		impl: logger,
	}
}

func (v *Logger) Log(_ context.Context, level sqlLog.Level, msg string, data map[string]any) {
	var logLevel log.Level

	switch level {
	case sqlLog.LevelError:
		logLevel = log.ErrorLevel
	case sqlLog.LevelInfo:
		logLevel = log.InfoLevel
	case sqlLog.LevelDebug:
		logLevel = log.DebugLevel
	case sqlLog.LevelTrace:
		logLevel = log.TraceLevel
	default:
		logLevel = log.DebugLevel
	}

	if v.impl.Level() >= logLevel {
		logger := v.impl

		if len(data) > 0 {
			logContext := logger.New()

			keys := make([]string, 0, 32)
			for key := range data {
				keys = append(keys, key)
			}

			slices.Sort(keys)

			for i := range keys {
				logContext = logContext.WithField(keys[i], data[keys[i]])
			}

			logger = logContext.Logger()
			defer logger.Release()
		}

		logger.Log(logLevel, msg)
	}
}
