package oracle

import (
	"errors"
	"net/url"
	"regexp"
)

type DSN string

var dsnRegexp = regexp.MustCompile(`^oracle://(.+):(.+)@(.*)$`)

func (v *DSN) UnmarshalText(text []byte) error {
	dsn := string(text)

	// To avoid a double escape
	if unescaped, err := url.QueryUnescape(string(text)); err == nil {
		dsn = unescaped
	}

	// Escape special characters
	m := dsnRegexp.FindStringSubmatch(dsn)
	if len(m) != 4 {
		return errors.New("invalid value")
	}

	*v = DSN("oracle://" + url.QueryEscape(m[1]) + ":" + url.QueryEscape(m[2]) + "@" + m[3])

	return nil
}
