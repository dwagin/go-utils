package oracle_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-utils/database"
	"gitlab.com/dwagin/go-utils/oracle"
)

func TestSanitize(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		name     string
		value    string
		expected string
	}{
		{
			name:     "Simple",
			value:    `column1`,
			expected: `"COLUMN1"`,
		},
		{
			name:     "Contain 0 byte",
			value:    `col` + string([]byte{0}) + `umn1`,
			expected: `"COLUMN1"`,
		},
		{
			name:     "Contain \" symbol",
			value:    `"column1"`,
			expected: `"""COLUMN1"""`,
		},
	}

	for i := range testCases {
		test := testCases[i]

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			actual := oracle.Sanitize(test.value)
			require.Equal(t, test.expected, actual)
		})
	}
}

func BenchmarkSanitize(b *testing.B) {
	testCases := []struct {
		name  string
		value string
	}{
		{
			name:  "Simple",
			value: `column1`,
		},
		{
			name:  "Contain 0 byte",
			value: `col` + string([]byte{0}) + `umn1`,
		},
		{
			name:  "Contain \" symbol",
			value: `"column1"`,
		},
	}

	b.Run("Old", func(b *testing.B) {
		for i := range testCases {
			test := testCases[i]
			b.Run(test.name, func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					_ = database.Sanitize(test.value)
				}
			})
		}
	})
	b.Run("New", func(b *testing.B) {
		for i := range testCases {
			test := testCases[i]
			b.Run(test.name, func(b *testing.B) {
				b.ReportAllocs()
				b.ResetTimer()

				for range b.N {
					_ = oracle.Sanitize(test.value)
				}
			})
		}
	})
}
