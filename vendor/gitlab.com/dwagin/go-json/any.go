package json

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/dwagin/go-bytebuffer"
)

type Appender interface {
	AppendJSON(dst []byte) []byte
}

//nolint:funlen,gocyclo,cyclop // special case
func Append(dst []byte, value any) []byte {
	if value == nil {
		return append(dst, Null...)
	}

	switch value := value.(type) {
	case bool:
		dst = AppendBool(dst, value)
	case string:
		dst = AppendString(dst, value)
	case error:
		dst = AppendString(dst, value.Error())
	case int:
		dst = AppendInt(dst, value)
	case int8:
		dst = AppendInt(dst, value)
	case int16:
		dst = AppendInt(dst, value)
	case int32:
		dst = AppendInt(dst, value)
	case int64:
		dst = AppendInt(dst, value)
	case uint:
		dst = AppendUint(dst, value)
	case uint8:
		dst = AppendUint(dst, value)
	case uint16:
		dst = AppendUint(dst, value)
	case uint32:
		dst = AppendUint(dst, value)
	case uint64:
		dst = AppendUint(dst, value)
	case float32:
		dst = AppendFloat32(dst, value)
	case float64:
		dst = AppendFloat64(dst, value)
	case []bool:
		dst = AppendSliceBool(dst, value)
	case []string:
		dst = AppendSliceString(dst, value)
	case []int:
		dst = AppendSliceInt(dst, value)
	case []int8:
		dst = AppendSliceInt(dst, value)
	case []int16:
		dst = AppendSliceInt(dst, value)
	case []int32:
		dst = AppendSliceInt(dst, value)
	case []int64:
		dst = AppendSliceInt(dst, value)
	case []uint:
		dst = AppendSliceUint(dst, value)
	case []uint8:
		dst = AppendSliceUint(dst, value)
	case []uint16:
		dst = AppendSliceUint(dst, value)
	case []uint32:
		dst = AppendSliceUint(dst, value)
	case []uint64:
		dst = AppendSliceUint(dst, value)
	case []float32:
		dst = AppendSliceFloat32(dst, value)
	case []float64:
		dst = AppendSliceFloat64(dst, value)
	case []any:
		dst = AppendSlice(dst, value)
	case map[string]any:
		dst = AppendMap(dst, value)
	case time.Duration:
		dst = AppendDuration(dst, value)
	case time.Time:
		dst = AppendTime(dst, value)
	case []time.Duration:
		dst = AppendSliceDuration(dst, value)
	case []time.Time:
		dst = AppendSliceTime(dst, value)
	case Appender:
		dst = value.AppendJSON(dst)
	case fmt.Stringer:
		dst = AppendStringer(dst, value)
	case json.RawMessage:
		dst = append(dst, value...)
	default:
		bb := bytebuffer.Get()
		defer bb.Release()

		// Use a json.Encoder to avoid escaping HTML.
		enc := json.NewEncoder(bb)
		enc.SetEscapeHTML(false)

		if err := enc.Encode(value); err != nil {
			return AppendString(dst, err.Error())
		}

		bb.B = bytes.TrimRight(bb.B, "\n")

		if len(bb.B) == 0 {
			return append(dst, Null...)
		}

		dst = append(dst, bb.B...)
	}

	return dst
}
