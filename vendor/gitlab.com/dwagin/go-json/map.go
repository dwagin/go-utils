package json

import "slices"

func AppendMap(dst []byte, m map[string]any) []byte {
	if m == nil {
		return append(dst, Null...)
	}

	if len(m) == 0 {
		return append(dst, EmptyObject...)
	}

	keys := make([]string, 0, 32)
	for key := range m {
		keys = append(keys, key)
	}

	slices.Sort(keys)

	dst = append(dst, '{')

	for i := range keys {
		dst = AppendString(dst, keys[i])
		dst = append(dst, ':')
		dst = Append(dst, m[keys[i]])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = '}'

	return dst
}
