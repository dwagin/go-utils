package json

import (
	"time"
)

//nolint:funlen // special case
func AppendDuration(dst []byte, d time.Duration) []byte {
	dst = append(dst, Quote)

	src := uint64(d) //nolint:gosec // safe conversion

	if d < 0 {
		dst = append(dst, '-')
		src = -src
	}

	if src >= uint64(time.Second) {
		start := false

		if src >= uint64(time.Hour) {
			tmp := src / uint64(time.Hour)
			src %= uint64(time.Hour)

			dst = AppendUint(dst, tmp)
			dst = append(dst, 'h')

			start = true
		}

		if src >= uint64(time.Minute) {
			tmp := src / uint64(time.Minute)
			src %= uint64(time.Minute)

			dst = appendSmallNums(dst, int(tmp)) //nolint:gosec // overflow is not possible
			dst = append(dst, 'm')

			start = true
		} else if start {
			dst = append(dst, "0m"...)
		}

		if src >= uint64(time.Second) {
			tmp := src / uint64(time.Second)
			src %= uint64(time.Second)

			dst = appendSmallNums(dst, int(tmp)) //nolint:gosec // overflow is not possible
		} else if start {
			dst = append(dst, '0')
		}

		if src > 0 {
			dst = append(dst, '.')
			div := uint64(time.Second / 10)

			for src > 0 && div > 0 {
				tmp := src / div
				src %= div

				dst = append(dst, byte(0x30|tmp))

				div /= 10
			}
		}

		dst = append(dst, 's')
	} else {
		switch {
		case src == 0:
			dst = append(dst, "0s"...)
		case src >= uint64(time.Millisecond):
			// print milliseconds
			tmp := src / uint64(time.Millisecond)
			src %= uint64(time.Millisecond)

			dst = AppendUint(dst, tmp)

			if src > 0 {
				dst = append(dst, '.')
				div := uint64(time.Millisecond) / 10

				for src > 0 && div > 0 {
					tmp = src / div
					src %= div

					dst = append(dst, byte(0x30|tmp))

					div /= 10
				}
			}

			dst = append(dst, "ms"...)
		case src >= uint64(time.Microsecond):
			// print microseconds
			tmp := src / uint64(time.Microsecond)
			src %= uint64(time.Microsecond)

			dst = AppendUint(dst, tmp)

			if src > 0 {
				dst = append(dst, '.')
				div := uint64(time.Microsecond) / 10

				for src > 0 && div > 0 {
					tmp = src / div
					src %= div

					dst = append(dst, byte(0x30|tmp))

					div /= 10
				}
			}

			// U+00B5 'µ' micro sign == 0xC2 0xB5, two bytes.
			dst = append(dst, "µs"...)
		default:
			// print nanoseconds
			dst = AppendUint(dst, src)
			dst = append(dst, "ns"...)
		}
	}

	dst = append(dst, Quote)

	return dst
}
