package json

import (
	"fmt"
	"unicode/utf8"
)

const hex = "0123456789abcdef"

var safeSet = [utf8.RuneSelf]bool{
	' ':      true,
	'!':      true,
	'"':      false,
	'#':      true,
	'$':      true,
	'%':      true,
	'&':      true,
	'\'':     true,
	'(':      true,
	')':      true,
	'*':      true,
	'+':      true,
	',':      true,
	'-':      true,
	'.':      true,
	'/':      true,
	'0':      true,
	'1':      true,
	'2':      true,
	'3':      true,
	'4':      true,
	'5':      true,
	'6':      true,
	'7':      true,
	'8':      true,
	'9':      true,
	':':      true,
	';':      true,
	'<':      true,
	'=':      true,
	'>':      true,
	'?':      true,
	'@':      true,
	'A':      true,
	'B':      true,
	'C':      true,
	'D':      true,
	'E':      true,
	'F':      true,
	'G':      true,
	'H':      true,
	'I':      true,
	'J':      true,
	'K':      true,
	'L':      true,
	'M':      true,
	'N':      true,
	'O':      true,
	'P':      true,
	'Q':      true,
	'R':      true,
	'S':      true,
	'T':      true,
	'U':      true,
	'V':      true,
	'W':      true,
	'X':      true,
	'Y':      true,
	'Z':      true,
	'[':      true,
	'\\':     false,
	']':      true,
	'^':      true,
	'_':      true,
	'`':      true,
	'a':      true,
	'b':      true,
	'c':      true,
	'd':      true,
	'e':      true,
	'f':      true,
	'g':      true,
	'h':      true,
	'i':      true,
	'j':      true,
	'k':      true,
	'l':      true,
	'm':      true,
	'n':      true,
	'o':      true,
	'p':      true,
	'q':      true,
	'r':      true,
	's':      true,
	't':      true,
	'u':      true,
	'v':      true,
	'w':      true,
	'x':      true,
	'y':      true,
	'z':      true,
	'{':      true,
	'|':      true,
	'}':      true,
	'~':      true,
	'\u007f': true,
}

func AppendString[Bytes []byte | string](dst []byte, src Bytes) []byte {
	dst = append(dst, Quote)

	// Portions of the string that contain no escapes are appended as
	// byte slices.

	offset := 0 // last non-escape symbol

	for idx := 0; idx < len(src); {
		if c := src[idx]; c < utf8.RuneSelf {
			if safeSet[c] { // single-width character, no escaping is required
				idx++

				continue
			}

			dst = append(dst, src[offset:idx]...)

			switch c {
			case '\\':
				dst = append(dst, `\\`...)
			case '"':
				dst = append(dst, `\"`...)
			case '\n':
				dst = append(dst, `\n`...)
			case '\r':
				dst = append(dst, `\r`...)
			case '\t':
				dst = append(dst, `\t`...)
			default:
				// This encodes bytes < 0x20 except for \t, \n and \r.
				// If escapeHTML is set, it also escapes <, >, and &
				// because they can lead to security holes when
				// user-controlled strings are rendered into JSON
				// and served to some browsers.
				dst = append(dst, '\\', 'u', '0', '0', hex[c>>4], hex[c&0xf])
			}

			idx++
			offset = idx

			continue
		}

		// TODO: Use generic utf8 functionality (https://go.dev/issue/56948).
		// For now, cast only a small portion of byte slices to a string
		// so that it can be stack allocated. This slows down []byte slightly
		// due to the extra copy, but keeps string performance roughly the same.
		n := len(src) - idx
		if n > utf8.UTFMax {
			n = utf8.UTFMax
		}

		c, size := utf8.DecodeRuneInString(string(src[idx : idx+n]))
		if c == utf8.RuneError && size == 1 {
			dst = append(dst, src[offset:idx]...)
			dst = append(dst, `\ufffd`...)

			idx += size
			offset = idx

			continue
		}

		// U+2028 is LINE SEPARATOR.
		// U+2029 is PARAGRAPH SEPARATOR.
		// They are both technically valid characters in JSON strings,
		// but don't work in JSONP, which has to be evaluated as JavaScript,
		// and can lead to security holes there. It is valid JSON to
		// escape them, so we do so unconditionally.
		// See http://timelessrepo.com/json-isnt-a-javascript-subset for discussion.
		if c == '\u2028' || c == '\u2029' {
			dst = append(dst, src[offset:idx]...)
			dst = append(dst, '\\', 'u', '2', '0', '2', hex[c&0xF])

			idx += size
			offset = idx

			continue
		}

		idx += size
	}

	dst = append(dst, src[offset:]...)

	dst = append(dst, Quote)

	return dst
}

func AppendStringer(dst []byte, src fmt.Stringer) []byte {
	if src == nil {
		return append(dst, Null...)
	}

	return AppendString(dst, src.String())
}
