package json

import (
	"time"

	"golang.org/x/exp/constraints"
)

func AppendSlice(dst []byte, s []any) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = Append(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceBool(dst []byte, s []bool) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendBool(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceString(dst []byte, s []string) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendString(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceInt[T constraints.Signed](dst []byte, s []T) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendInt(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceUint[T constraints.Unsigned](dst []byte, s []T) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendUint(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceFloat32(dst []byte, s []float32) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendFloat32(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceFloat64(dst []byte, s []float64) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendFloat64(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceDuration(dst []byte, s []time.Duration) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendDuration(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}

func AppendSliceTime(dst []byte, s []time.Time) []byte {
	if s == nil {
		return append(dst, Null...)
	}

	if len(s) == 0 {
		return append(dst, EmptyArray...)
	}

	dst = append(dst, '[')

	for i := range s {
		dst = AppendTime(dst, s[i])
		dst = append(dst, ',')
	}

	dst[len(dst)-1] = ']'

	return dst
}
