package log

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/mattn/go-colorable"
	"gitlab.com/dwagin/go-bytebuffer"
)

const (
	colorBlack = iota + 30
	colorRed
	colorGreen
	colorYellow
	colorBlue
	colorMagenta
	colorCyan
	colorWhite

	colorBold     = 1
	colorDarkGray = 90
)

type console struct {
	writer io.Writer
	cache  *bytebuffer.ByteBuffer
	once   sync.Once
}

var consolePool sync.Pool

func acquireConsole() *console {
	if item := consolePool.Get(); item != nil {
		return item.(*console) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(console)
}

func Console() Writer {
	w := acquireConsole()

	w.writer = colorable.NewColorable(os.Stdout)

	return w
}

func (v *console) New() Writer {
	w := acquireConsole()

	w.writer = v.writer

	return w
}

func (v *console) WriteEvent(level Level, name string, msg []byte, fields *Fields) {
	bb := bbPool.Get()

	bb.B = beginColor(bb.B, colorDarkGray)
	bb.B = time.Now().AppendFormat(bb.B, time.RFC3339Nano)
	bb.B = endColor(bb.B)
	bb.B = append(bb.B, "        "...)
	bb.B = bb.B[:45]

	switch level {
	case TraceLevel:
		bb.B = colorize(bb.B, "TRACE ", colorMagenta)
	case DebugLevel:
		bb.B = colorize(bb.B, "DEBUG ", colorYellow)
	case InfoLevel:
		bb.B = colorize(bb.B, "INFO  ", colorGreen)
	case NoticeLevel:
		bb.B = colorize(bb.B, "NOTICE", colorGreen)
	case WarnLevel:
		bb.B = colorize(bb.B, "WARN  ", colorRed)
	case ErrorLevel:
		bb.B = colorize2(bb.B, "ERROR ", colorBold, colorRed)
	case FatalLevel:
		bb.B = colorize2(bb.B, "FATAL ", colorBold, colorRed)
	case PanicLevel:
		bb.B = colorize2(bb.B, "PANIC ", colorBold, colorRed)
	}

	bb.B = append(bb.B, ' ')
	bb.B = colorize2(bb.B, name, colorBold, colorCyan)
	bb.B = append(bb.B, ": "...)
	bb.B = append(bb.B, msg...)

	v.once.Do(func() {
		if fields != nil {
			s := *fields
			v.cache = bbPool.Get()

			for i := range s {
				v.cache.B = append(v.cache.B, ' ')
				v.cache.B = colorize(v.cache.B, s[i].Key, colorBlue)
				v.cache.B = append(v.cache.B, '=')
				v.cache.B = fmt.Append(v.cache.B, s[i].Value)
			}
		}
	})

	if v.cache != nil {
		bb.B = append(bb.B, v.cache.B...)
	}

	bb.B = append(bb.B, "\n"...)

	v.writer.Write(bb.B) //nolint:errcheck // error ignored

	bb.Release()
}

func (v *console) Close() error {
	return nil
}

func (v *console) Release() {
	if v == nil {
		panic("console: not defined")
	}

	if v.cache != nil {
		v.cache.Release()
		v.cache = nil
	}

	v.writer = nil
	v.once = sync.Once{}

	consolePool.Put(v)
}

func colorize[Bytes []byte | string](dst []byte, msg Bytes, c int) []byte {
	dst = beginColor(dst, c)
	dst = append(dst, msg...)
	dst = endColor(dst)

	return dst
}

func colorize2[Bytes []byte | string](dst []byte, msg Bytes, c1 int, c2 int) []byte {
	dst = beginColor(dst, c1)
	dst = beginColor(dst, c2)
	dst = append(dst, msg...)
	dst = endColor(dst)
	dst = endColor(dst)

	return dst
}

func beginColor(dst []byte, c int) []byte {
	dst = append(dst, "\x1b["...)
	dst = strconv.AppendInt(dst, int64(c), 10)
	dst = append(dst, 'm')

	return dst
}

func endColor(dst []byte) []byte {
	return append(dst, "\x1b[0m"...)
}
