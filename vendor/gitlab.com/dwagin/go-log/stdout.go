package log

import (
	"io"
	"os"
	"sync"
	"time"

	"gitlab.com/dwagin/go-bytebuffer"
	"gitlab.com/dwagin/go-json"
)

type stdout struct {
	writer io.Writer
	cache  *bytebuffer.ByteBuffer
	once   sync.Once
}

var stdoutPool sync.Pool

func acquireStdout() *stdout {
	if item := stdoutPool.Get(); item != nil {
		return item.(*stdout) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(stdout)
}

func Stdout() Writer {
	w := acquireStdout()

	w.writer = os.Stdout

	return w
}

func (v *stdout) New() Writer {
	w := acquireStdout()

	w.writer = v.writer

	return w
}

func (v *stdout) WriteEvent(level Level, name string, msg []byte, fields *Fields) {
	bb := bbPool.Get()

	bb.B = append(bb.B, `{"@timestamp":`...)
	bb.B = json.AppendTime(bb.B, time.Now())

	bb.B = append(bb.B, `,"log.level":`...)
	bb.B = level.AppendJSON(bb.B)

	bb.B = append(bb.B, `,"log.logger":`...)
	bb.B = json.AppendString(bb.B, name)

	bb.B = append(bb.B, `,"message":`...)
	bb.B = json.AppendString(bb.B, msg)

	v.once.Do(func() {
		if fields != nil {
			v.cache = bbPool.Get()
			v.cache.B = fields.AppendJSON(v.cache.B)
		}
	})

	if v.cache != nil {
		bb.B = append(bb.B, `,"labels":{`...)
		bb.B = append(bb.B, v.cache.B...)
		bb.B = append(bb.B, '}')
	}

	bb.B = append(bb.B, ",\"ecs.version\":\""+ecsVersion+"\"}\n"...)

	v.writer.Write(bb.B) //nolint:errcheck // error ignored

	bb.Release()
}

func (v *stdout) Close() error {
	return nil
}

func (v *stdout) Release() {
	if v == nil {
		panic("stdout: not defined")
	}

	if v.cache != nil {
		v.cache.Release()
		v.cache = nil
	}

	v.writer = nil
	v.once = sync.Once{}

	stdoutPool.Put(v)
}
