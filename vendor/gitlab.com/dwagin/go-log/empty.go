package log

import (
	"sync"
)

type empty struct{}

var emptyPool sync.Pool

func acquireEmpty() *empty {
	if item := emptyPool.Get(); item != nil {
		return item.(*empty) //nolint:errcheck,forcetypeassert // redundant
	}

	return new(empty)
}

func Empty() Writer {
	return acquireEmpty()
}

func (v *empty) New() Writer {
	return acquireEmpty()
}

func (v *empty) WriteEvent(Level, string, []byte, *Fields) {}

func (v *empty) Close() error {
	return nil
}

func (v *empty) Release() {
	if v == nil {
		panic("empty: not defined")
	}

	emptyPool.Put(v)
}
