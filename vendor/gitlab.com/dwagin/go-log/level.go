package log

import (
	"fmt"
	"strings"

	syslogBase "gitlab.com/dwagin/go-syslog"
)

//nolint:recvcheck // special case
type Level uint8

const (
	// PanicLevel level, the highest level of severity.
	// Logs and then calls panic with the message passed to Debug, Info, ...
	PanicLevel Level = iota
	// FatalLevel level. Used for critical entries, after which the program usually exits.
	FatalLevel
	// ErrorLevel level. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ErrorLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel
	// NoticeLevel level. Conditions that are not error conditions, but should possibly be handled specially.
	NoticeLevel
	// InfoLevel level. General operational entries about what's going on inside the application.
	InfoLevel
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel
	// TraceLevel level. Designates finer-grained informational events than the Debug.
	TraceLevel
)

func (v Level) String() string {
	switch v {
	case PanicLevel:
		return "panic"
	case FatalLevel:
		return "fatal"
	case ErrorLevel:
		return "error"
	case WarnLevel:
		return "warning"
	case NoticeLevel:
		return "notice"
	case InfoLevel:
		return "info"
	case DebugLevel:
		return "debug"
	case TraceLevel:
		return "trace"
	default:
		return "unknown"
	}
}

func (v Level) AppendJSON(dst []byte) []byte {
	switch v {
	case PanicLevel:
		dst = append(dst, `"panic"`...)
	case FatalLevel:
		dst = append(dst, `"fatal"`...)
	case ErrorLevel:
		dst = append(dst, `"error"`...)
	case WarnLevel:
		dst = append(dst, `"warning"`...)
	case NoticeLevel:
		dst = append(dst, `"notice"`...)
	case InfoLevel:
		dst = append(dst, `"info"`...)
	case DebugLevel:
		dst = append(dst, `"debug"`...)
	case TraceLevel:
		dst = append(dst, `"trace"`...)
	default:
		dst = append(dst, `"unknown"`...)
	}

	return dst
}

// MarshalText implements encoding.TextMarshaler.
func (v Level) MarshalText() ([]byte, error) {
	switch v {
	case PanicLevel:
		return []byte("panic"), nil
	case FatalLevel:
		return []byte("fatal"), nil
	case ErrorLevel:
		return []byte("error"), nil
	case WarnLevel:
		return []byte("warning"), nil
	case NoticeLevel:
		return []byte("notice"), nil
	case InfoLevel:
		return []byte("info"), nil
	case DebugLevel:
		return []byte("debug"), nil
	case TraceLevel:
		return []byte("trace"), nil
	default:
		return nil, fmt.Errorf("unknown log level: %d", v)
	}
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (v *Level) UnmarshalText(text []byte) error {
	switch strings.ToLower(string(text)) {
	case "panic":
		*v = PanicLevel
	case "fatal":
		*v = FatalLevel
	case "error":
		*v = ErrorLevel
	case "warn", "warning":
		*v = WarnLevel
	case "notice":
		*v = NoticeLevel
	case "info":
		*v = InfoLevel
	case "debug":
		*v = DebugLevel
	case "trace":
		*v = TraceLevel
	default:
		return fmt.Errorf("unknown log level: %q", string(text))
	}

	return nil
}

func (v Level) SyslogPriority() syslogBase.Priority {
	switch v {
	case PanicLevel, FatalLevel:
		return syslogBase.LOG_CRIT
	case ErrorLevel:
		return syslogBase.LOG_ERR
	case WarnLevel:
		return syslogBase.LOG_WARNING
	case NoticeLevel:
		return syslogBase.LOG_NOTICE
	case InfoLevel:
		return syslogBase.LOG_INFO
	case DebugLevel, TraceLevel:
		return syslogBase.LOG_DEBUG
	default:
		return syslogBase.LOG_DEBUG
	}
}
