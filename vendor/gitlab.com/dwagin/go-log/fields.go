package log

import (
	"sync"

	"gitlab.com/dwagin/go-json"
)

type Field struct {
	Value any
	Key   string
}

type Fields []Field

var fieldsPool sync.Pool

func AcquireFields(size int) *Fields {
	if item := fieldsPool.Get(); item != nil {
		fields := item.(*Fields) //nolint:errcheck,forcetypeassert // redundant
		if cap(*fields) >= size {
			return fields
		}
	}

	fields := make(Fields, 0, size)

	return &fields
}

func (v *Fields) Clone() *Fields {
	res := AcquireFields(len(*v))
	*res = append(*res, *v...)

	return res
}

func (v *Fields) Append(key string, value any) {
	l, c := len(*v), cap(*v)

	if l == c {
		*v = append(*AcquireFields(l + 1), *v...)
	}

	*v = append(*v, Field{
		Key:   key,
		Value: value,
	})
}

func (v *Fields) AppendJSON(dst []byte) []byte {
	if v == nil || len(*v) == 0 {
		return dst
	}

	s := *v

	for i := range s {
		dst = json.AppendString(dst, s[i].Key)
		dst = append(dst, ':')
		dst = json.Append(dst, s[i].Value)
		dst = append(dst, ',')
	}

	dst = dst[:len(dst)-1]

	return dst
}

func (v *Fields) Release() {
	if v == nil {
		panic("Fields: not defined")
	}

	clear(*v)

	*v = (*v)[:0]

	fieldsPool.Put(v)
}
