package envconfig

import (
	"encoding"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func parseStruct(prefix string, value reflect.Value) (bool, error) {
	mutated := false

	if value.Kind() != reflect.Struct {
		return false, errors.New("structure expected")
	}

	valueType := value.Type()

field:
	for i := range value.NumField() {
		field := value.Field(i)
		structField := valueType.Field(i)
		structTag := structField.Tag

		if !field.CanSet() {
			if structTag.Get(TagEnv) != "" {
				return false, fmt.Errorf("%s: cannot parse private fields", structField.Name)
			}

			continue field
		}

		for field.Kind() == reflect.Pointer {
			if field.IsNil() {
				break
			}

			field = field.Elem()
		}

		if field.Kind() == reflect.Pointer {
			root := reflect.New(field.Type().Elem())

			newValue := root
			for newValue.Kind() == reflect.Pointer {
				if newValue.IsNil() {
					newValue.Set(reflect.New(newValue.Type().Elem()))
				}

				newValue = newValue.Elem()

				if newValue.Kind() == reflect.Struct {
					if _, ok := specialStructs[newValue.Type()]; !ok {
						nested := structTag.Get(TagEnv)
						if nested != "" {
							nested += EnvSeparator
						}

						res, err := parseStruct(prefix+nested, newValue)
						if err != nil {
							return false, err
						}

						if res {
							mutated = true
							field.Set(root)
						}

						continue field
					}
				}
			}
		}

		if field.Kind() == reflect.Struct {
			if _, ok := specialStructs[field.Type()]; !ok {
				nested := structTag.Get(TagEnv)
				if nested != "" {
					nested += EnvSeparator
				}

				res, err := parseStruct(prefix+nested, field)
				if err != nil {
					return false, err
				}

				if res {
					mutated = true
				}

				continue field
			}
		}

		if env, ok := structTag.Lookup(TagEnv); ok {
			env = prefix + env
			if rawValue, ok := os.LookupEnv(env); ok {
				if err := parseValue(field, rawValue, structTag.Get(TagEnvTimeLayout)); err != nil {
					return false, fmt.Errorf("parsing field %v from env %v error: %v", structField.Name, env, err)
				}

				mutated = true
			}
		}
	}

	return mutated, nil
}

type parseFunc func(reflect.Value, string, string) error

var specialStructs = map[reflect.Type]parseFunc{
	reflect.TypeOf(time.Time{}): func(field reflect.Value, str string, timeLayout string) error {
		if timeLayout == "" {
			timeLayout = time.RFC3339
		}

		val, err := time.Parse(timeLayout, str)
		if err != nil {
			return fmt.Errorf("parse time error: %v", err)
		}

		field.Set(reflect.ValueOf(val))

		return nil
	},

	reflect.TypeOf(time.Location{}): func(field reflect.Value, str string, _ string) error {
		val, err := time.LoadLocation(str)
		if err != nil {
			return fmt.Errorf("parse time location error: %v", err)
		}

		field.Set(reflect.ValueOf(*val))

		return nil
	},

	reflect.TypeOf(url.URL{}): func(field reflect.Value, str string, _ string) error {
		val, err := url.Parse(str)
		if err != nil {
			return fmt.Errorf("parse URL error: %v", err)
		}

		field.Set(reflect.ValueOf(*val))

		return nil
	},
}

//nolint:cyclop,funlen // exceptional situation
func parseValue(value reflect.Value, raw string, timeLayout string) error {
	// Handle pointers and uninitialized pointers
	for value.Type().Kind() == reflect.Pointer {
		if value.IsNil() {
			value.Set(reflect.New(value.Type().Elem()))
		}

		value = value.Elem()
	}

	if value.CanAddr() {
		if u, ok := value.Addr().Interface().(encoding.TextUnmarshaler); ok {
			err := u.UnmarshalText([]byte(raw))
			if err != nil {
				return fmt.Errorf("unmarshal text error: %v", err)
			}

			return nil
		}

		if u, ok := value.Addr().Interface().(json.Unmarshaler); ok {
			err := u.UnmarshalJSON([]byte(raw))
			if err != nil {
				return fmt.Errorf("unmarshal json error: %v", err)
			}

			return nil
		}
	}

	valueType := value.Type()

	//nolint:exhaustive // unsupported type
	switch valueType.Kind() {
	case reflect.Bool:
		parseBool, err := strconv.ParseBool(raw)
		if err != nil {
			return fmt.Errorf("parse bool error: %v", err)
		}

		value.SetBool(parseBool)

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32:
		parseInt, err := strconv.ParseInt(raw, 0, valueType.Bits())
		if err != nil {
			return fmt.Errorf("parse int error: %v", err)
		}

		value.SetInt(parseInt)

	case reflect.Int64:
		if valueType == reflect.TypeOf(time.Duration(0)) {
			parseDuration, err := time.ParseDuration(raw)
			if err != nil {
				return fmt.Errorf("parse duration error: %v", err)
			}

			value.SetInt(int64(parseDuration))
		} else {
			parseInt, err := strconv.ParseInt(raw, 0, valueType.Bits())
			if err != nil {
				return fmt.Errorf("parse int error: %v", err)
			}

			value.SetInt(parseInt)
		}

	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		parseUint, err := strconv.ParseUint(raw, 0, valueType.Bits())
		if err != nil {
			return fmt.Errorf("parse uint error: %v", err)
		}

		value.SetUint(parseUint)

	case reflect.Float32, reflect.Float64:
		parseFloat, err := strconv.ParseFloat(raw, valueType.Bits())
		if err != nil {
			return fmt.Errorf("parse float error: %v", err)
		}

		value.SetFloat(parseFloat)

	case reflect.Map:
		mapValue, err := parseMap(valueType, raw, timeLayout)
		if err != nil {
			return err
		}

		value.Set(*mapValue)

	case reflect.Slice:
		sliceValue, err := parseSlice(valueType, raw, timeLayout)
		if err != nil {
			return err
		}

		value.Set(*sliceValue)

	case reflect.String:
		value.SetString(raw)

	case reflect.Struct:
		if parser, ok := specialStructs[valueType]; ok {
			return parser(value, raw, timeLayout)
		}

	default:
		return errors.New("unsupported type")
	}

	return nil
}

func parseSlice(valueType reflect.Type, raw string, timeLayout string) (*reflect.Value, error) {
	if valueType.Elem().Kind() == reflect.Uint8 {
		sliceValue := reflect.ValueOf([]byte(raw))

		return &sliceValue, nil
	}

	split := strings.Split(raw, ",")
	sliceValue := reflect.MakeSlice(valueType, len(split), len(split))

	for i := range split {
		err := parseValue(sliceValue.Index(i), strings.TrimSpace(split[i]), timeLayout)
		if err != nil {
			return nil, err
		}
	}

	return &sliceValue, nil
}

func parseMap(valueType reflect.Type, raw string, timeLayout string) (*reflect.Value, error) {
	var err error

	split := strings.Split(raw, ",")
	mapValue := reflect.MakeMapWithSize(valueType, len(split))

	for i := range split {
		kv := strings.SplitN(split[i], ":", 2)
		if len(kv) != 2 {
			return nil, fmt.Errorf("invalid map item: %q", split[i])
		}

		k := reflect.New(valueType.Key()).Elem()
		if err = parseValue(k, strings.TrimSpace(kv[0]), timeLayout); err != nil {
			return nil, err
		}

		v := reflect.New(valueType.Elem()).Elem()
		if err = parseValue(v, strings.TrimSpace(kv[1]), timeLayout); err != nil {
			return nil, err
		}

		mapValue.SetMapIndex(k, v)
	}

	return &mapValue, nil
}
