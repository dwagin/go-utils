package http

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"
)

type ClientConfig struct {
	Transport *TransportConfig `env:"TRANSPORT" yaml:"transport"`
	Timeout   *int             `env:"TIMEOUT" yaml:"timeout"`
}

func (v *ClientConfig) Client() (*http.Client, error) {
	var err error

	client := &http.Client{}

	if v.Transport != nil {
		if client.Transport, err = v.Transport.Transport(); err != nil {
			return nil, fmt.Errorf("transport config error: %v", err)
		}
	}

	if v.Timeout != nil {
		client.Timeout = time.Duration(*v.Timeout) * time.Millisecond
	}

	return client, nil
}

func (v *ClientConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Transport != nil {
		if err := v.Transport.Validate(); err != nil {
			return fmt.Errorf("'transport' not valid: %v", err)
		}
	}

	if v.Timeout != nil && *v.Timeout < 0 {
		return errors.New("'timeout' invalid value")
	}

	return nil
}

func DrainBody(r *http.Response) {
	io.Discard.(io.ReaderFrom).ReadFrom(r.Body) //nolint:errcheck,forcetypeassert // error ignored
	r.Body.Close()
}
