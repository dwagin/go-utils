package http

import (
	"errors"
	"fmt"
	"net"
	"net/http"
	"time"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

type TransportConfig struct {
	DialTimeout           *int                   `env:"DIAL_TIMEOUT" yaml:"dial_timeout"`
	TLSConfig             *utilsTLS.ClientConfig `env:"TLS_CONFIG" yaml:"tls_config"`
	TLSHandshakeTimeout   *int                   `env:"TLS_HANDSHAKE_TIMEOUT" yaml:"tls_handshake_timeout"`
	MaxIdleConns          *int                   `env:"MAX_IDLE_CONNS" yaml:"max_idle_conns"`
	MaxIdleConnsPerHost   *int                   `env:"MAX_IDLE_CONNS_PER_HOST" yaml:"max_idle_conns_per_host"`
	IdleConnTimeout       *int                   `env:"IDLE_CONN_TIMEOUT" yaml:"idle_conn_timeout"`
	ResponseHeaderTimeout *int                   `env:"RESPONSE_HEADER_TIMEOUT" yaml:"response_header_timeout"`
	ForceAttemptHTTP2     *bool                  `env:"FORCE_ATTEMPT_HTTP_2" yaml:"force_attempt_http2"`
}

func (v *TransportConfig) Transport() (*http.Transport, error) {
	var err error

	transport := &http.Transport{
		TLSHandshakeTimeout:   time.Duration(10) * time.Second,
		MaxIdleConns:          100,
		IdleConnTimeout:       time.Duration(90) * time.Second,
		ExpectContinueTimeout: time.Duration(1) * time.Second,
		ForceAttemptHTTP2:     true,
	}

	if v.DialTimeout != nil {
		dialer := &net.Dialer{
			Timeout:   time.Duration(*v.DialTimeout) * time.Millisecond,
			KeepAlive: time.Duration(30) * time.Second,
		}
		transport.DialContext = dialer.DialContext
	} else {
		dialer := &net.Dialer{
			Timeout:   time.Duration(30) * time.Second,
			KeepAlive: time.Duration(30) * time.Second,
		}
		transport.DialContext = dialer.DialContext
	}

	if v.TLSConfig != nil {
		if transport.TLSClientConfig, err = v.TLSConfig.TLSConfig(); err != nil {
			return nil, fmt.Errorf("tls config error: %v", err)
		}
	}

	if v.TLSHandshakeTimeout != nil {
		transport.TLSHandshakeTimeout = time.Duration(*v.TLSHandshakeTimeout) * time.Millisecond
	}

	if v.MaxIdleConns != nil {
		transport.MaxIdleConns = *v.MaxIdleConns
	}

	if v.MaxIdleConnsPerHost != nil {
		transport.MaxIdleConnsPerHost = *v.MaxIdleConnsPerHost
	}

	if v.IdleConnTimeout != nil {
		transport.IdleConnTimeout = time.Duration(*v.IdleConnTimeout) * time.Millisecond
	}

	if v.ResponseHeaderTimeout != nil {
		transport.ResponseHeaderTimeout = time.Duration(*v.ResponseHeaderTimeout) * time.Millisecond
	}

	if v.ForceAttemptHTTP2 != nil {
		transport.ForceAttemptHTTP2 = *v.ForceAttemptHTTP2
	}

	return transport, nil
}

func (v *TransportConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.DialTimeout != nil && *v.DialTimeout < 0 {
		return errors.New("'dial_timeout' invalid value")
	}

	if v.TLSHandshakeTimeout != nil && *v.TLSHandshakeTimeout < 0 {
		return errors.New("'tls_handshake_timeout' invalid value")
	}

	if v.MaxIdleConns != nil && *v.MaxIdleConns < 0 {
		return errors.New("'max_idle_conns' invalid value")
	}

	if v.MaxIdleConnsPerHost != nil && *v.MaxIdleConnsPerHost < 0 {
		return errors.New("'max_idle_conns_per_host' invalid value")
	}

	if v.IdleConnTimeout != nil && *v.IdleConnTimeout < 0 {
		return errors.New("'idle_conn_timeout' invalid value")
	}

	if v.ResponseHeaderTimeout != nil && *v.ResponseHeaderTimeout < 0 {
		return errors.New("'response_header_timeout' invalid value")
	}

	return nil
}
