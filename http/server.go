package http

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

//nolint:govet // config structure
type ServerConfig struct {
	Address           string                 `env:"ADDRESS" yaml:"address"`
	Port              uint16                 `env:"PORT" yaml:"port"`
	TLSConfig         *utilsTLS.ServerConfig `env:"TLS_CONFIG" yaml:"tls_config"`
	ReadHeaderTimeout *int                   `env:"READ_HEADER_TIMEOUT" yaml:"read_header_timeout"`
	IdleTimeout       *int                   `env:"IDLE_TIMEOUT" yaml:"idle_timeout"`
}

func (v *ServerConfig) Server() (*http.Server, error) {
	var err error

	server := &http.Server{
		Addr:              fmt.Sprintf("%s:%d", v.Address, v.Port),
		ReadHeaderTimeout: time.Duration(5000) * time.Millisecond,
	}

	if v.TLSConfig != nil {
		if server.TLSConfig, err = v.TLSConfig.TLSConfig(); err != nil {
			return nil, fmt.Errorf("tls config error: %v", err)
		}
	}

	if v.ReadHeaderTimeout != nil {
		server.ReadHeaderTimeout = time.Duration(*v.ReadHeaderTimeout) * time.Millisecond
	}

	if v.IdleTimeout != nil {
		server.IdleTimeout = time.Duration(*v.IdleTimeout) * time.Millisecond
	}

	return server, nil
}

func (v *ServerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Address == "" {
		return errors.New("'address' not defined")
	}

	if v.Port < 1 {
		return errors.New("'port' invalid value")
	}

	if v.ReadHeaderTimeout != nil && *v.ReadHeaderTimeout < 0 {
		return errors.New("'read_header_timeout' invalid value")
	}

	if v.IdleTimeout != nil && *v.IdleTimeout < 0 {
		return errors.New("'idle_timeout' invalid value")
	}

	if v.IdleTimeout != nil && *v.IdleTimeout < 1000 {
		return errors.New("'idle_timeout' too small")
	}

	return nil
}
