package kafka

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/twmb/franz-go/pkg/kgo"
)

//nolint:govet // config structure
type ConsumerConfig struct {
	ConsumeResetOffset string   `env:"CONSUME_RESET_OFFSET" yaml:"consume_reset_offset"`
	ConsumerGroup      string   `env:"CONSUMER_GROUP" yaml:"consumer_group"`
	ConsumeTopics      []string `env:"CONSUME_TOPICS" yaml:"consume_topics"`
	InstanceID         string   `env:"INSTANCE_ID" yaml:"instance_id"`
	Rack               string   `env:"RACK" yaml:"rack"`
	SessionTimeout     *int     `env:"SESSION_TIMEOUT" yaml:"session_timeout"`
	RebalanceTimeout   *int     `env:"REBALANCE_TIMEOUT" yaml:"rebalance_timeout"`
}

func (v *ConsumerConfig) Opts(opts []kgo.Opt) ([]kgo.Opt, error) {
	if v == nil {
		return opts, nil
	}

	opts = append(opts,
		kgo.ConsumerGroup(v.ConsumerGroup),
		kgo.ConsumeTopics(v.ConsumeTopics...),
		kgo.RequireStableFetchOffsets(),
		kgo.FetchIsolationLevel(kgo.ReadCommitted()),
		kgo.Rack(v.Rack),
	)

	switch strings.ToLower(v.ConsumeResetOffset) {
	case "start":
		opts = append(opts, kgo.ConsumeResetOffset(kgo.NewOffset().AtStart()))
	case "end":
		opts = append(opts, kgo.ConsumeResetOffset(kgo.NewOffset().AtEnd()))
	default:
		return nil, fmt.Errorf("unrecognized 'consume_reset_offset': %q", v.ConsumeResetOffset)
	}

	if v.InstanceID != "" {
		opts = append(opts, kgo.InstanceID(v.InstanceID))
	}

	if v.SessionTimeout != nil {
		opts = append(opts, kgo.SessionTimeout(time.Duration(*v.SessionTimeout)*time.Millisecond))
	}

	if v.RebalanceTimeout != nil {
		opts = append(opts, kgo.RebalanceTimeout(time.Duration(*v.RebalanceTimeout)*time.Millisecond))
	}

	return opts, nil
}

func (v *ConsumerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.ConsumerGroup == "" {
		return errors.New("'consumer_group' must be defined")
	}

	if len(v.ConsumeTopics) < 1 {
		return errors.New("'consume_topics' must be defined")
	}

	if v.SessionTimeout != nil && *v.SessionTimeout < 0 {
		return errors.New("'session_timeout' invalid value")
	}

	if v.RebalanceTimeout != nil && *v.RebalanceTimeout < 0 {
		return errors.New("'rebalance_timeout' invalid value")
	}

	return nil
}
