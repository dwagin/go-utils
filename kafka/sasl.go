package kafka

import (
	"errors"
	"fmt"
	"strings"

	"github.com/twmb/franz-go/pkg/kgo"
	"github.com/twmb/franz-go/pkg/sasl/aws"
	"github.com/twmb/franz-go/pkg/sasl/plain"
	"github.com/twmb/franz-go/pkg/sasl/scram"
)

type SASLConfig struct {
	Method   string `env:"METHOD" yaml:"method"`
	User     string `env:"USER" yaml:"user"`
	Password string `env:"PASSWORD" yaml:"password"`
	TLS      bool   `env:"TLS" yaml:"tls"`
}

func (v *SASLConfig) Opts(opts []kgo.Opt) ([]kgo.Opt, error) {
	if v == nil {
		return opts, nil
	}

	switch strings.ToLower(v.Method) {
	case "plain":
		opts = append(opts, kgo.SASL(plain.Auth{
			User: v.User,
			Pass: v.Password,
		}.AsMechanism()))
	case "scram-sha-256":
		opts = append(opts, kgo.SASL(scram.Auth{
			User: v.User,
			Pass: v.Password,
		}.AsSha256Mechanism()))
	case "scram-sha-512":
		opts = append(opts, kgo.SASL(scram.Auth{
			User: v.User,
			Pass: v.Password,
		}.AsSha512Mechanism()))
	case "aws-msk-iam", "aws_msk_iam":
		opts = append(opts, kgo.SASL(aws.Auth{
			AccessKey: v.User,
			SecretKey: v.Password,
			UserAgent: "franz-go",
		}.AsManagedStreamingIAMMechanism()))
	default:
		return nil, fmt.Errorf("unrecognized 'method': %q", v.Method)
	}

	return opts, nil
}

func (v *SASLConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Method == "" {
		return errors.New("'method' must be defined")
	}

	if v.User == "" {
		return errors.New("'user' must be defined")
	}

	if v.Password == "" {
		return errors.New("'password' must be defined")
	}

	return nil
}
