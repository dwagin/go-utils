package kafka

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/twmb/franz-go/pkg/kgo"
)

type ProducerConfig struct {
	DefaultTopic          string   `env:"DEFAULT_TOPIC" yaml:"default_topic"`
	Linger                *int     `env:"LINGER" yaml:"linger"`
	RecordDeliveryTimeout *int     `env:"RECORD_DELIVERY_TIMEOUT" yaml:"record_delivery_timeout"`
	RecordRetries         *int     `env:"RECORD_RETRIES" yaml:"record_retries"`
	TransactionTimeout    *int     `env:"TRANSACTION_TIMEOUT" yaml:"transaction_timeout"`
	TransactionalID       string   `env:"TRANSACTIONAL_ID" yaml:"transactional_id"`
	Compression           []string `env:"COMPRESSION" yaml:"compression"`
}

func (v *ProducerConfig) Opts(opts []kgo.Opt) ([]kgo.Opt, error) {
	if v == nil {
		return opts, nil
	}

	opts = append(opts, kgo.DefaultProduceTopic(v.DefaultTopic))

	if v.TransactionTimeout != nil {
		opts = append(opts, kgo.TransactionTimeout(time.Duration(*v.TransactionTimeout)*time.Millisecond))
	}

	if v.Linger != nil {
		opts = append(opts, kgo.ProducerLinger(time.Duration(*v.Linger)*time.Millisecond))
	}

	if v.RecordDeliveryTimeout != nil {
		opts = append(opts, kgo.RecordDeliveryTimeout(time.Duration(*v.RecordDeliveryTimeout)*time.Millisecond))
	}

	if v.RecordRetries != nil {
		opts = append(opts, kgo.RecordRetries(*v.RecordRetries))
	}

	if v.TransactionalID != "" {
		transactionalID := v.TransactionalID + "-" + strconv.FormatInt(int64(os.Getpid()), 10)
		opts = append(opts, kgo.TransactionalID(transactionalID))
	}

	var codecs []kgo.CompressionCodec

	if len(v.Compression) > 0 {
		for i := range v.Compression {
			switch strings.ToLower(v.Compression[i]) {
			case "none":
				codecs = append(codecs, kgo.NoCompression())
			case "gzip":
				codecs = append(codecs, kgo.GzipCompression())
			case "snappy":
				codecs = append(codecs, kgo.SnappyCompression())
			case "lz4":
				codecs = append(codecs, kgo.Lz4Compression())
			case "zstd":
				codecs = append(codecs, kgo.ZstdCompression())
			default:
				return nil, fmt.Errorf("unrecognized 'producer.compression' codec: %q", v.Compression[i])
			}
		}
	} else {
		codecs = []kgo.CompressionCodec{kgo.ZstdCompression(), kgo.NoCompression()}
	}

	opts = append(opts, kgo.ProducerBatchCompression(codecs...))

	return opts, nil
}

func (v *ProducerConfig) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.Linger != nil && *v.Linger < 0 {
		return errors.New("'linger' invalid value")
	}

	if v.RecordDeliveryTimeout != nil && *v.RecordDeliveryTimeout < 0 {
		return errors.New("'record_delivery_timeout' invalid value")
	}

	if v.RecordRetries != nil && *v.RecordRetries < 1 {
		return errors.New("'record_retries' invalid value")
	}

	if v.TransactionTimeout != nil && *v.TransactionTimeout < 0 {
		return errors.New("'transaction_timeout' invalid value")
	}

	if v.TransactionalID == "" {
		return errors.New("'transactional_id' must be defined")
	}

	return nil
}
