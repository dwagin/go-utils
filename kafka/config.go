package kafka

import (
	"crypto/tls"
	"errors"
	"fmt"
	"time"

	"github.com/twmb/franz-go/pkg/kgo"
	"gitlab.com/dwagin/go-log"

	utilsTLS "gitlab.com/dwagin/go-utils/tls"
)

//nolint:govet // config structure
type Config struct {
	LogLevel    *log.Level             `env:"LOG_LEVEL" yaml:"log_level"`
	SeedBrokers []string               `env:"SEED_BROKERS" yaml:"seed_brokers"`
	DialTimeout *int                   `env:"DIAL_TIMEOUT" yaml:"dial_timeout"`
	Consumer    *ConsumerConfig        `env:"CONSUMER" yaml:"consumer"`
	Producer    *ProducerConfig        `env:"PRODUCER" yaml:"producer"`
	SASL        *SASLConfig            `env:"SASL" yaml:"sasl"`
	TLSConfig   *utilsTLS.ClientConfig `env:"TLS_CONFIG" yaml:"tls_config"`
}

func (v *Config) Opts(logger *log.Logger) ([]kgo.Opt, error) {
	var err error

	opts := []kgo.Opt{
		kgo.SeedBrokers(v.SeedBrokers...),
		kgo.DialTimeout(7 * time.Second),
	}

	if logger != nil {
		if v.LogLevel != nil {
			logger = logger.New().WithLevel(*v.LogLevel).Logger()
		}

		opts = append(opts, kgo.WithLogger(newLogger(logger)))
	}

	if v.DialTimeout != nil {
		opts = append(opts, kgo.DialTimeout(time.Duration(*v.DialTimeout)*time.Millisecond))
	}

	if opts, err = v.Consumer.Opts(opts); err != nil {
		return nil, fmt.Errorf("consumer error: %v", err)
	}

	if opts, err = v.Producer.Opts(opts); err != nil {
		return nil, fmt.Errorf("producer error: %v", err)
	}

	if v.SASL != nil {
		if opts, err = v.SASL.Opts(opts); err != nil {
			return nil, fmt.Errorf("sasl error: %v", err)
		}

		if v.SASL.TLS && v.TLSConfig == nil {
			opts = append(opts, kgo.DialTLS())
		}
	}

	if v.TLSConfig != nil {
		var tlsConfig *tls.Config

		if tlsConfig, err = v.TLSConfig.TLSConfig(); err != nil {
			return nil, fmt.Errorf("tls config error: %v", err)
		}

		opts = append(opts, kgo.DialTLSConfig(tlsConfig))
	}

	return opts, nil
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if v.DialTimeout != nil && *v.DialTimeout < 0 {
		return errors.New("'dial_timeout' invalid value")
	}

	if v.Consumer == nil && v.Producer == nil {
		return errors.New("'consumer' or 'producer' must be defined")
	}

	if v.Consumer != nil {
		if err := v.Consumer.Validate(); err != nil {
			return fmt.Errorf("'consumer' not valid: %v", err)
		}
	}

	if v.Producer != nil {
		if err := v.Producer.Validate(); err != nil {
			return fmt.Errorf("'producer' not valid: %v", err)
		}
	}

	if v.SASL != nil {
		if err := v.SASL.Validate(); err != nil {
			return fmt.Errorf("'sasl' not valid: %v", err)
		}
	}

	return nil
}
