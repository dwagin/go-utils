package kafka

import (
	"github.com/twmb/franz-go/pkg/kgo"
	"gitlab.com/dwagin/go-log"
)

type Logger struct {
	impl *log.Logger
}

func newLogger(logger *log.Logger) *Logger {
	return &Logger{
		impl: logger,
	}
}

func (v *Logger) Log(level kgo.LogLevel, msg string, kv ...any) {
	var logLevel log.Level

	switch level {
	case kgo.LogLevelNone:
		return
	case kgo.LogLevelError:
		logLevel = log.ErrorLevel
	case kgo.LogLevelWarn:
		logLevel = log.WarnLevel
	case kgo.LogLevelInfo:
		logLevel = log.InfoLevel
	case kgo.LogLevelDebug:
		logLevel = log.DebugLevel
	default:
		logLevel = log.DebugLevel
	}

	if v.impl.Level() >= logLevel {
		logger := v.impl

		if len(kv) > 1 {
			logContext := logger.New()

			for i := 0; i < len(kv); i += 2 {
				if key, ok := kv[i].(string); ok {
					logContext = logContext.WithField(key, kv[i+1])
				}
			}

			logger = logContext.Logger()
			defer logger.Release()
		}

		logger.Log(logLevel, msg)
	}
}

func (v *Logger) Level() kgo.LogLevel {
	switch v.impl.Level() {
	case log.PanicLevel:
		return kgo.LogLevelError
	case log.FatalLevel:
		return kgo.LogLevelError
	case log.ErrorLevel:
		return kgo.LogLevelError
	case log.WarnLevel:
		return kgo.LogLevelWarn
	case log.NoticeLevel:
		return kgo.LogLevelWarn
	case log.InfoLevel:
		return kgo.LogLevelInfo
	case log.DebugLevel:
		return kgo.LogLevelDebug
	case log.TraceLevel:
		return kgo.LogLevelDebug
	default:
		return kgo.LogLevelDebug
	}
}
