package unsafe_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/dwagin/go-utils/unsafe"
)

func TestStringToBytes(t *testing.T) {
	t.Parallel()

	bytes := unsafe.StringToBytes("")
	require.Nil(t, bytes)
}

func TestBytesToString(t *testing.T) {
	t.Parallel()

	str := unsafe.BytesToString(nil)
	require.Equal(t, "", str)
}
