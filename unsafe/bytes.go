package unsafe

import "unsafe"

// BytesToString returns a string pointer without allocation.
func BytesToString(b []byte) string {
	return unsafe.String(unsafe.SliceData(b), len(b))
}

// StringToBytes converts a string to a byte array to be used with string encoding functions.
//
// Note that the output byte array should not be modified if the input string
// is expected to be used again - doing so could violate Go semantics.
func StringToBytes(s string) []byte {
	// unsafe.StringData output is unspecified for empty string input so always
	// return nil.
	if len(s) == 0 {
		return nil
	}

	return unsafe.Slice(unsafe.StringData(s), len(s))
}
