package s3proxy

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	utilsHTTP "gitlab.com/dwagin/go-utils/http"
)

type S3Proxy struct {
	client   *http.Client
	endpoint string
}

func (v *Config) New() (*S3Proxy, error) {
	client, err := v.Client.Client()
	if err != nil {
		return nil, fmt.Errorf("client error: %v", err)
	}

	return &S3Proxy{
		endpoint: strings.TrimRight(v.Endpoint, "/") + "/",
		client:   client,
	}, nil
}

func (v *S3Proxy) Put(ctx context.Context, reader io.Reader, objectName string, size int64) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodPut, v.endpoint+objectName, reader)
	if err != nil {
		return fmt.Errorf("create request error: %v", err)
	}

	req.ContentLength = size

	resp, err := v.client.Do(req)
	if err != nil {
		return fmt.Errorf("request error: %v", err)
	}
	defer resp.Body.Close()

	utilsHTTP.DrainBody(resp)

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return fmt.Errorf("put object '%s' error: status code %d", objectName, resp.StatusCode)
	}

	return nil
}

func (v *S3Proxy) Delete(ctx context.Context, objectName string) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodDelete, v.endpoint+objectName, nil)
	if err != nil {
		return fmt.Errorf("create request error: %v", err)
	}

	resp, err := v.client.Do(req)
	if err != nil {
		return fmt.Errorf("request error: %v", err)
	}
	defer resp.Body.Close()

	utilsHTTP.DrainBody(resp)

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return fmt.Errorf("delete object '%s' error: status code %d", objectName, resp.StatusCode)
	}

	return nil
}
