package s3proxy

import (
	"errors"
	"fmt"

	utilsHTTP "gitlab.com/dwagin/go-utils/http"
)

type Config struct {
	Client   *utilsHTTP.ClientConfig `env:"CLIENT" yaml:"client"`
	Endpoint string                  `env:"ENDPOINT" yaml:"endpoint"`
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if err := v.Client.Validate(); err != nil {
		return fmt.Errorf("'client' not valid: %v", err)
	}

	if v.Endpoint == "" {
		return errors.New("'endpoint' is empty")
	}

	return nil
}
