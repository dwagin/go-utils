package general

import (
	"errors"
	"fmt"

	utilsLogger "gitlab.com/dwagin/go-utils/logger"
)

type Config struct {
	Log *utilsLogger.Config `env:"LOG" yaml:"log"`
}

func (v *Config) Default() {
	*v = Config{
		Log: &utilsLogger.Config{},
	}

	v.Log.Default()
}

func (v *Config) Validate() error {
	if v == nil {
		return errors.New("not defined")
	}

	if err := v.Log.Validate(); err != nil {
		return fmt.Errorf("'log' not valid: %v", err)
	}

	return nil
}
